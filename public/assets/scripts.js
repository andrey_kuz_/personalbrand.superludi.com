//todo refactoring
console.log();
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

function accessCheck() {
    $.ajax({
        type: "post",
        url: "/access",
        success: function (data) {
            if (data == 'banned') {
                window.location.href = location.pathname;
            }
        }
    });
}

function IsJsonString(str) {
    try {
        return typeof str == 'object';
    } catch (e) {
        return false;
    }
    return true;
}

//cooke
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
        function (m, key, value) {
            vars[key] = value;
        });
    return vars;
}

//Get params
function CatchUTM() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    if (url.searchParams !== undefined) {
        var utm_source = url.searchParams.get("utm_source"),
            utm_medium = url.searchParams.get("utm_medium"),
            utm_campaign = url.searchParams.get("utm_campaign"),
            utm_term = url.searchParams.get("utm_term"),
            utm_content = url.searchParams.get("utm_content");
    } else {
        var params = getUrlVars(),
            utm_source = params["utm_source"],
            utm_medium = params["utm_medium"],
            utm_campaign = params["utm_campaign"],
            utm_term = params["utm_term"],
            utm_content = params["utm_content"];
    }
    var dateCookie = new Date;
    dateCookie.setDate(dateCookie.getDate() + 1);

    if (utm_source) {
        setCookie('utm_source', utm_source, {expires: dateCookie.toUTCString()});
    }
    if (utm_medium) {
        setCookie('utm_medium', utm_medium, {expires: dateCookie.toUTCString()});
    }
    if (utm_campaign) {
        setCookie('utm_campaign', utm_campaign, {expires: dateCookie.toUTCString()});
    }
    if (utm_term) {
        setCookie('utm_term', utm_term, {expires: dateCookie.toUTCString()});
    }
    if (utm_content) {
        setCookie('utm_content', utm_content, {expires: dateCookie.toUTCString()});
    }
}
CatchUTM();


function SimulationClick(link) {
    if (typeof link !== 'object') {
        return;
    }

    var linkEvent = null;
    if (document.createEvent) {
        linkEvent = document.createEvent('MouseEvents');
        linkEvent.initEvent('click', true, true);
        link.dispatchEvent(linkEvent);
    }
    else if (document.createEventObject) {
        linkEvent = document.createEventObject();
        link.fireEvent('onclick', linkEvent);
    }
}

var current_video_id = '';

iframes = [];
$(".adaptive-iframe").each(function () {
    iframes[$(this).attr('id')] = $(this).html();
    $(this).empty();
    $(this).hide();
    $(this).after('<div class="img-video" data-id="' + $(this).attr('id') + '"><img width="655" class="start_this_video"  src="https://img.youtube.com/vi/' + $(this).attr('id') + '/0.jpg" alt="" /><div class="play-video"></div></div>');
});
//ga start
$('.promo__content .promo__list .promo__item .promo__button.promo__button_buy.btn.scroll-link').click(function () {
    if (typeof (ga) === "function") {
        ga('send', {
            eventCategory: 'buy_button',
            eventAction: 'click'
        });
    }
});
$('.promo__content .promo__list .promo__item .promo__button.promo__button_watch.btn.btn-green.btn-bordered-watch.scroll-link').click(function () {
    if (typeof (ga) === "function") {
        ga('send', {
            eventCategory: 'free_lesson',
            eventAction: 'click'
        });
    }
});
$('.subscribe-box__form-group .subscribe-box__form-button.btn').click(function () {
    setTimeout(checkForm, 500);

});
function checkForm() {
    if ($('.subscribe-box__form-success').is(":visible")) {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'email_button',
                eventAction: 'submit'
            });
        }
    }
}
$('.packages__list .packages__list-item .package-box .package-box__footer a').click(function () {
    if ($(this).attr('href') == window.location.protocol + '//' + window.location.host + '/videos-to-pay/cheap') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'buy_button_good',
                eventAction: 'click'
            });
        }
    }
    if ($(this).attr('href') == window.location.protocol + '//' + window.location.host + '/videos-to-pay/good') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'buy_button_best',
                eventAction: 'click'
            });
        }
    }
    if ($(this).attr('href') == window.location.protocol + '//' + window.location.host + '/videos-to-pay/premium') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'buy_button_special-offer',
                eventAction: 'click'
            });
        }
    }
});

$('.packages__list .packages__list-item .package-box .package-box__footer a#package_cheap').click(function () {
    if (typeof (ga) === "function") {
        ga('send', {
            eventCategory: 'buy_button_good',
            eventAction: 'click'
        });
    }
    type_package = 'package_cheap';

});

$('.packages__list .packages__list-item .package-box .package-box__footer a#package_good').click(function () {
    if (typeof (ga) === "function") {
        ga('send', {
            eventCategory: 'buy_button_best',
            eventAction: 'click'
        });
    }
    type_package = 'package_good';

});

$('.packages__list .packages__list-item .package-box .package-box__footer a#package_premium').click(function () {
    if (typeof (ga) === "function") {
        ga('send', {
            eventCategory: 'buy_button_special-offer',
            eventAction: 'click'
        });
    }
    type_package = 'package_special-offer';
});

$('div#modal-buy-course .modal-dialog .modal-content .buy-box .validation-wrap form#checkout_step_first').submit(function () {
    setTimeout(checkFormBuy, 1000);
});
function checkFormBuy() {
    if (type_package == 'package_cheap') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'continue_button_good',
                    eventAction: 'submit'
                });
            }
        }
    } else if (type_package == 'package_good') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'continue_button_best',
                    eventAction: 'submit'
                });
            }
        }
    } else if (type_package == 'package_premium') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'continue_button_special-offer',
                    eventAction: 'submit'
                });
            }
        }
    }
}


$('.packages__list .packages__list-item .package-box .package-box__footer a.package-box__button').click(function () {
    if ($(this).prev().attr('id') == 'package_cheap') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'free_lesson_good',
                eventAction: 'click'
            });
        }
        type_package_free_lesson = 'package_cheap';
    }
    if ($(this).prev().attr('id') == 'package_good') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'free_lesson_ best',
                eventAction: 'click'
            });
        }
        type_package_free_lesson = 'package_good';
    }
    if ($(this).prev().attr('id') == 'package_premium') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'free_lesson_special-offer',
                eventAction: 'click'
            });
        }
        type_package_free_lesson = 'package_special-offer';
    }
});
$('div#modal-registration .modal-dialog .modal-content .enter-box .validation-wrap .enter-box__social.btn.btn-fb.btn-block').click(function () {
    if (type_package_free_lesson == 'package_cheap') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'registration_fb_good',
                eventAction: 'click'
            });
        }
    } else if (type_package_free_lesson == 'package_good') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'registration_fb_best',
                eventAction: 'click'

            });
        }
    } else if (type_package_free_lesson == 'package_premium') {
        if (typeof (ga) === "function") {
            ga('send', {
                eventCategory: 'registration_fb_special-offer',
                eventAction: 'click'
            });
        }
    }
});

$('div#modal-registration .modal-dialog .modal-content .enter-box .validation-wrap form#form_registration').submit(function () {
    setTimeout(checkFormRegister, 2000);
});
function checkFormRegister() {
    if (type_package_free_lesson == 'package_cheap') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.form-registration.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'registration_good',
                    eventAction: 'submit'
                });
            }
        }
    } else if (type_package_free_lesson == 'package_good') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.form-registration.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'registration_best',
                    eventAction: 'submit'
                });
            }
        }
    } else if (type_package_free_lesson == 'package_premium') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.form-registration.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'registration_special-offer',
                    eventAction: 'submit'
                });
            }
        }
    }
}
$('div#modal-phone .modal-dialog .modal-content .phone-box .validation-wrap form#form_request_phone').submit(function () {
    setTimeout(checkFormPhone, 2000);
});
function checkFormPhone() {
    if (type_package_free_lesson == 'package_cheap') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.form-request-phone.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'comein_good',
                    eventAction: 'submit'
                });
            }
        }
    } else if (type_package_free_lesson == 'package_good') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.form-request-phone.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'comein_best',
                    eventAction: 'submit'
                });
            }
        }
    } else if (type_package_free_lesson == 'package_premium') {
        if (($('.form-group.has-error').html() === undefined ) && ($('.server-message-box.form-request-phone.error').css('display') === undefined)) {
            if (typeof (ga) === "function") {
                ga('send', {
                    eventCategory: 'comein_special-offer',
                    eventAction: 'submit'
                });
            }
        }
    }
}
//ga end
$(document).ready(function () {

    $(".img-video").click(function () {
        iframe = iframes[$(this).attr('data-id')];
        $(".adaptive-iframe").empty();
        $(".adaptive-iframe").hide();
        $(".adaptive-iframe").next().show();
        $(this).prev().html(iframe);
        $(this).prev().show();
        //$(this).empty();
        $(this).hide();
    });

    $('#searchBtn').click(function () {
        $('#search').toggleClass('active');
        $('#searchBtn').toggleClass('active');
        $('#modal-search-field').focus();
    });

    //FIXED
    $(window).scroll(function () {
        if ($(window).width() >= '991') {
            // $('.blog__post-btn').css('transform', 'translateY(-' + $(window).scrollTop() + 'px)');
            $('.blog__post-info').css('transform', 'translateY(' + $(window).scrollTop() * 0.3 + 'px)');
            $('.blog__post-info').css('opacity', 1 - $(window).scrollTop() * 0.002);
            $('.post__head .blog__post img').css('filter', 'contrast(' + (1 - $(window).scrollTop() * 0.002) + ')');
            $('.post__head .blog__post img').css('transform', 'scale(' + (1 + $(window).scrollTop() * 0.0005) + ')');
        }

        if ($(window).scrollTop() >= $(window).height() * 0.45) {
            $('header').addClass('fixed');
            $(".fixed").css("overflow", "visible");
        } else {
            $('header').removeClass('fixed');
            $(".fixed").css("overflow", "hidden");
        }
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() >= $(window).height() * 0.45) {
            $('.blog-layout header').addClass('header_fixed header_transition header_show');
            // $(".fixed").css("overflow", "visible");
        } else {
            $('.blog-layout header').removeClass('header_fixed header_transition header_show');
            // $(".fixed").css("overflow", "hidden");
        }
    });
    function serverMessageBoxClear(server_message_box) {
        if (typeof server_message_box !== 'object') {
            return;
        }

        server_message_box.hide();
        server_message_box.html('');
        server_message_box.removeClass('error');
    }

    function ValidPayForm(data) {
        return typeof data === 'object' &&
            'action' in data && data.action.length &&
            'key' in data && data.key.length &&
            'payment' in data && data.payment.length &&
            'order' in data && data.order.length &&
            'data' in data && data.data.length &&
            'sign' in data && data.sign.length
            ;
    }

    function SetPayForm(data) {
        if (!$('#pay_form').length || !ValidPayForm(data) || !$('#pay_form')[0].hasAttribute('action')) {
            return false;
        }

        $('#pay_form').attr('action', data.action);
        if ($('#pay_form').find('input[name="key"]').length) {
            $('#pay_form').find('input[name="key"]').val(data.key);
        }
        if ($('#pay_form').find('input[name="payment"]').length) {
            $('#pay_form').find('input[name="payment"]').val(data.payment);
        }
        if ($('#pay_form').find('input[name="order"]').length) {
            $('#pay_form').find('input[name="order"]').val(data.order);
        }
        if ($('#pay_form').find('input[name="data"]').length) {
            $('#pay_form').find('input[name="data"]').val(data.data);
        }
        if ($('#pay_form').find('input[name="sign"]').length) {
            $('#pay_form').find('input[name="sign"]').val(data.sign);
        }
        if ($('#pay_form').find('input[name="ext1"]').length) {
            $('#pay_form').find('input[name="ext1"]').val(data.ext1);
        }

        return true;
    }

    function OnPay(user_id, video_id) {
        if (!$('#modal-buy-course').length || !$('#modal_buy_course_second_step_show').length || !$('#pay_form').length) {
            return;
        }

        var server_message_box = $("#modal-buy-course-second-step").find('.server-message-box');
        serverMessageBoxClear(server_message_box);

        $('#modal-buy-course').find('.modal-close.close').trigger("click");
        SimulationClick($('#modal_buy_course_second_step_show')[0]);


        $.ajax({
            type: 'POST',
            url: $("#checkout_step_first").attr('action-paying'),
            data: {user_id: user_id, video_id: video_id},
            dataType: 'json',
            success: function (data) {

                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    $('#modal-buy-course-second-step').find('.js-message-pay-in-progress').hide();
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else if (data.success && 'form' in data && SetPayForm(data.form)) {
                    $('#pay_form').submit();
                }

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    }

    $('[data-search]').on('click', function () {
        $('.header__search').find('').toggle(300);
    });

    $("#checkout_step_first").on('submit', function (e) {
        var server_message_box;
        if ($(this).find('.server-message-box').length) {
            server_message_box = $(this).find('.server-message-box');
            serverMessageBoxClear(server_message_box);
        }
        if (!$(this).find('input[name="user_name"]').length || !$(this).find('input[name="user_phone"]').length || !$(this).find('input[name="user_email"]').length || !server_message_box) {
            return;
        }

        if ($(this).find('.has-error').length > 0) {
            return;
        }

        var user_name = $(this).find('input[name="user_name"]').val();
        var user_phone = $(this).find('input[name="user_phone"]').val();
        var user_email = $(this).find('input[name="user_email"]').val();
        if (!user_name.length || !user_phone.length || !user_email.length) {
            return;
        }

        $.ajax({
            type: 'POST',
            url: $("#checkout_step_first").attr('action'),
            data: {name: user_name, phone: user_phone, email: user_email},
            dataType: 'json',
            success: function (data) {
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    server_message_box.addClass('error');
                }
                else {
                    $("#checkout_step_first").trigger("reset");
                    setTimeout(function () {
                        serverMessageBoxClear(server_message_box);
                        OnPay(data.user_id, current_video_id);
                    }, 1000);
                }

                server_message_box.html(data.message);
                server_message_box.fadeIn('slow');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        return false;
    });

    $('a[href="#modal-buy-course"], a[href="#modal-buy-course-second-step"]').on('click', function (e) {

        if ($(this)[0].hasAttribute('course-all')) {
            current_video_id = $(this).attr('course-all');
        }

    });


    $('#login_form').bind('submit', function (e) {
        var server_message_box;
        if ($(this).find('.server-message-box').length) {
            server_message_box = $(this).find('.server-message-box');
            serverMessageBoxClear(server_message_box);
        }
        if (!$(this).find('input[name="user_email"]').length || !$(this).find('input[name="user_password"]').length || !server_message_box) {
            return;
        }

        if ($(this).find('.has-error').length > 0) {
            return;
        }

        var user_email = $(this).find('input[name="user_email"]').val();
        var user_password = $(this).find('input[name="user_password"]').val();

        if (!user_email.length || !user_password.length) {
            return;
        }

        $.ajax({
            type: 'POST',
            beforeSend: function (request) {
                request.setRequestHeader("Access-Control-Allow-Origin", document.location.origin);
            },
            url: $('#login_form').attr('action'),
            data: {email: user_email, password: user_password},
            dataType: 'json',
            success: function (data) {
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    server_message_box.addClass('error');
                }
                else {
                    $('#login_form').trigger("reset");
                    setTimeout(function () {
                        serverMessageBoxClear(server_message_box);
                        $('#login_form').find('.modal-close.close').trigger("click");
                        window.location.href = data.redirect;
                    }, 1500);
                }

                server_message_box.html(data.message);
                server_message_box.fadeIn('slow');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        return false;
    });

    function AutoShowPopup() {
        if (typeof window.location_hash === 'undefined' || !window.location_hash.length || !$('a[href="' + window.location_hash + '"]').length) {
            return;
        }

        setTimeout(function () {
            SimulationClick($('a[href="' + window.location_hash + '"]')[0]);
        }, 500);
    }

    AutoShowPopup();

    function AutoShowOrderToPay() {
        if (typeof window.order_to_pay === 'undefined' || !window.order_to_pay.length || !$('#' + window.order_to_pay).length) {
            return;
        }

        setTimeout(function () {
            SimulationClick($('#' + window.order_to_pay)[0]);
        }, 500);
    }

    AutoShowOrderToPay();

    $('a[href="#modal-buy-course-second-step"]').on('click', function (e) {
        if ($(this)[0].hasAttribute('id') && $(this).attr('id') == 'modal_buy_course_second_step_show') {
            return;
        }

        var server_message_box = $('#modal-buy-course-second-step').find('.server-message-box');
        serverMessageBoxClear(server_message_box);

        $.ajax({
            type: 'POST',
            url: $("#checkout_step_first").attr('action-paying'),
            data: {video_id: current_video_id},
            dataType: 'json',
            success: function (data) {
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    $('#modal-buy-course-second-step').find('.js-message-pay-in-progress').hide();
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else if (data.success && 'form' in data && SetPayForm(data.form)) {
                    $('#pay_form').submit();
                }

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $("#form_pass_recovery").on('submit', function (e) {
        var server_message_box;
        if ($(this).find('.server-message-box.form-pass-recovery').length) {
            server_message_box = $(this).find('.server-message-box.form-pass-recovery');
            serverMessageBoxClear(server_message_box);
        }

        if (!$(this).find('input[name="user_email"]').length || !$('#modal-pass-recovery').length || !$('a[href="#modal-pass-success"]').length || !server_message_box
        ) {
            return;
        }

        if ($(this).find('.has-error').length > 0) {
            return;
        }

        var user_email = $(this).find('input[name="user_email"]').val();
        if (!user_email.length) {
            return;
        }

        $.ajax({
            type: 'POST',
            url: $("#form_pass_recovery").attr('action'),
            data: {email: user_email},
            dataType: 'json',
            success: function (data) {
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else {
                    $("#form_pass_recovery").trigger("reset");
                    $('#modal-pass-recovery').find('.modal-close.close').trigger("click");
                    SimulationClick($('a[href="#modal-pass-success"]')[0]);
                }

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        return false;
    });

    function RedirectToPurchasedVideo() {
        if (!('url_purchased_video' in window) || !window.url_purchased_video.length) {
            return;
        }

        setTimeout(function () {
            window.location.href = window.url_purchased_video;
        }, 2000);
    }

    RedirectToPurchasedVideo();

    function OnRequestPhone() {
        var server_message_box;
        if ($("#form_request_phone").find('.server-message-box').length) {
            server_message_box = $("#form_request_phone").find('.server-message-box');
            serverMessageBoxClear(server_message_box);
        }

        if (!('request_phone' in window) || !$("#form_request_phone").find('input[name="user_phone"]').length || !server_message_box
        ) {
            return;
        }

        if ($("#form_request_phone").find('.has-error').length > 0) {
            return;
        }

        var user_phone = $("#form_request_phone").find('input[name="user_phone"]').val();
        if (!user_phone.length) {
            return;
        }

        $("#form_request_phone_button").addClass('not-active');
        var data = {
            name: window.request_phone.name,
            email: window.request_phone.email,
            phone: user_phone,
            is_auto_login: 1
        };
        if ('password' in window.request_phone) {
            data.password = window.request_phone.password;
        }
        $.ajax({
            type: 'POST',
            url: $("#form_request_phone").attr('action'),
            data: data,
            dataType: 'json',
            success: function (data) {
                $("#form_request_phone_button").removeClass('not-active');
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else if (data.success) {
                    $('#form_request_phone').trigger("reset");
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                    setTimeout(function () {
                        window.location.href = window.request_phone.redirect;
                    }, 1500);

                }

            },
            error: function (data) {
                $("#form_request_phone_button").removeClass('not-active');
                console.log('Error:', data);
            }
        });
    }

    if ($("#form_request_phone").length) {
        setInterval(function () {
            if ($("#form_request_phone").parent().hasClass('submitted')) {
                $("#form_request_phone").parent().removeClass('submitted');
                OnRequestPhone();
            }
        }, 50);
    }

    function OnRegistration() {
        var server_message_box;
        if ($("#form_registration").find('.server-message-box').length) {
            server_message_box = $("#form_registration").find('.server-message-box');
            serverMessageBoxClear(server_message_box);
        }

        if (!$("#form_registration").find('input[name="user_email"]').length || !$("#form_registration").find('input[name="user_password"]').length || !server_message_box
        ) {
            return;
        }

        if ($("#form_registration").find('.has-error').length > 0) {
            return;
        }

        var user_email = $("#form_registration").find('input[name="user_email"]').val();
        if (!user_email.length) {
            return;
        }

        var user_password = $("#form_registration").find('input[name="user_password"]').val();
        if (!user_password.length) {
            return;
        }

        $("#form_registration_button").addClass('not-active');
        $.ajax({
            type: 'POST',
            url: $("#form_registration").attr('action'),
            data: {email: user_email, password: user_password},
            dataType: 'json',
            success: function (data) {
                $("#form_registration_button").removeClass('not-active');
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else if (data.success) {
                    $('#form_registration').trigger("reset");
                    $('#modal-registration').find('.modal-close.close').trigger("click");
                    window.request_phone = data.user_info;
                    $('#modal_phone_user_label').text(window.request_phone.name);
                    SimulationClick($('#modal_phone_show')[0]);
                }

            },
            error: function (data) {
                $("#form_registration_button").removeClass('not-active');
                console.log('Error:', data);
            }
        });
    }

    if ($("#form_registration").length) {
        setInterval(function () {
            if ($("#form_registration").parent().hasClass('submitted')) {
                $("#form_registration").parent().removeClass('submitted');
                OnRegistration();
            }
        }, 50);
    }
    $('.nav__trigger').click(function () {
        $('.search-btn.active').click();
    });

    $('.search-btn').click(function () {
        $('.is-active').removeClass('is-active');
    });
});

var navigation = {
    // Variables
    $nav: document.querySelector('.nav'),
    $navTrigger: document.querySelector('.nav__trigger'),
    $navContent: document.querySelector('.nav__content'),
    $navList: document.querySelector('.nav__list'),
    transitionEnd: 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',

    init: function init() {
        var self = this;

        if (!self.$navTrigger) return;

        // Handle the transitions
        self.$navTrigger.addEventListener('click', function () {
            if (!self.$navTrigger.classList.contains('is-active')) {
                // .nav--trigger active
                self.$navTrigger.classList.add('is-active');

                // .nav active
                if (!self.$nav.classList.contains('is-active')) {
                    self.$nav.classList.add('is-active');
                    self.$nav.addEventListener('transitionend', function (e) {
                        if (e.propertyName == 'width' && self.$navTrigger.classList.contains('is-active')) {
                            // .nav__content active
                            self.$navContent.classList.add('is-active');
                        }
                    });
                } else {
                    self.$navContent.classList.add('is-active');
                }

                // no-csstransitions fallback
                if (document.documentElement.classList.contains('no-csstransitions')) {
                    self.$navContent.classList.add('is-active');
                }
            } else {
                // .nav--trigger inactive
                self.$navTrigger.classList.remove('is-active');

                // .nav__content inactive
                if (self.$navContent.classList.contains('is-active')) {
                    self.$navContent.classList.remove('is-active');
                    self.$navContent.addEventListener('transitionend', function (e) {
                        if (e.propertyName == 'opacity' && !self.$navTrigger.classList.contains('is-active')) {
                            // .nav inactive
                            self.$nav.classList.remove('is-active');
                        }
                    });
                } else {
                    self.$nav.classList.remove('is-active');
                }

                // no-csstransitions fallback
                if (document.documentElement.classList.contains('no-csstransitions')) {
                    self.$nav.classList.remove('is-active');
                }
            }
        });
    }
};

navigation.init();

$(document).ready(function () {
    function save_point(video) {
        var time = 0;
        video_id = $(".videos__video.active.in").attr('id');
        if (video_id !== undefined) {
            time = Math.round(video.time());
            if (time) {
                $.ajax({
                    type: "POST",
                    url: "/video_point",
                    data: "time=" + time + "&video_id=" + video_id,
                    success: function (msg) {
                    }
                });
            }
        }
    }

    accessCheck();
    setInterval(accessCheck, 15000);

    $(".content-text img[alt]").each(function () {
        var $img = $(this);
        if ($img.attr('alt')) {
            $(this).after("<figcaption>" + $img.attr('alt') + "</figcaption>");
        }
    });

  $('.videos__list .videos__preview-wrap .video-paid.videos__preview').click(function () {
        window._wq = window._wq || [];
        _wq.push({
            id: "_all", onReady: function (video) {
                save_point(video);
            }
        });

    });

    window._wq = window._wq || [];
    _wq.push({
        id: "_all", onReady: function (video) {
            var time = $(".videos__video.active.in").attr('video-point');
            video.time(time);
            video.pause();
            video.unmute();
            iframe = document.querySelector('.videos__video.fade.active iframe');
            video.bind("pause", function () {
                save_point(video);
            });
        }
    });

});
<?php

session_start();

require_once('classes/AmoCrm.php');

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name = clearData($_POST['first_name']);
$email = clearData($_POST['email']);
$phone = clearData($_POST['custom_mobile_phone']);

$utmSource = clearData($_SESSION['utm_source']);
$utmMedium = clearData($_SESSION['utm_medium']);
$utmCampaign = clearData($_SESSION['utm_campaign']);
$utmTerm = clearData($_SESSION['utm_term']);
$utmContent = clearData($_SESSION['utm_content']);

if(!empty($name) && !empty($email)) {

    // Save user in crm
    $amoCrm = new AmoCrm([
        'USER_LOGIN' => 'alex1tropic@gmail.com',
        'USER_HASH'  => 'b8ae3635a98554be36223d62bedae7a0'
    ], 'alekseyzarutskiy');

    $lead = $amoCrm->storeLead('Регистрация на вебинар', 20030446, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent);

    $leadId = $lead['response']['leads']['add'][0]['id'];

    $amoCrm->storeContact($name, $leadId, $email, $phone);

    $rv = ['success' => true];

} else {

    $rv = ['success' => false];
}

echo json_encode($rv);


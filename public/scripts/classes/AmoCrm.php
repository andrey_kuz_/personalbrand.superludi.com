<?php

class AmoCrm
{
    private $user = [];
    private $subDomain;

    /**
     * AmoCrm constructor.
     * @param array $user
     * @param $subDomain
     */
    public function __construct(array $user, $subDomain)
    {
        $this->user = $user;
        $this->subDomain = $subDomain;

        $this->curlInit('https://'. $this->subDomain . '.amocrm.ru/private/api/auth.php?type=json', $user);
    }

    /**
     * @param $link
     * @param $data
     * @return mixed
     */
    private function curlInit($link, $data)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return json_decode($out, true);
    }

    /**
     * @param $title
     * @param $statusId
     * @param $utmSource
     * @param $utmMedium
     * @param $utmCampaign
     * @param $utmTerm
     * @param $utmContent
     * @return mixed
     */
    public function storeLead($title, $statusId, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent)
    {

        $leads['request']['leads']['add'] = array(
            array(
                'name'        => $title,
                'date_create' => time(),
                'status_id'   => $statusId,
                'custom_fields' => array(

                    array(
                        'id'        => 471215,
                        'values'    => array(
                            array(
                                'value' => $utmSource
                            )
                        )
                    ),
                    array(
                        'id'        => 471217,
                        'values'    => array(
                            array(
                                'value' => $utmMedium
                            )
                        )
                    ),
                    array(
                        'id'        => 471219,
                        'values'    => array(
                            array(
                                'value' => $utmCampaign
                            )
                        )
                    ),
                    array(
                        'id'        => 471221,
                        'values'    => array(
                            array(
                                'value' => $utmTerm
                            )
                        )
                    ),
                    array(
                        'id'        => 471223,
                        'values'    => array(
                            array(
                                'value' => $utmContent
                            )
                        )
                    )
                )
            )

        );

        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/leads/set', $leads);
    }

    /**
     * @param $leadId
     * @param $statusId
     * @return mixed
     */
    public function updateLead($leadId, $statusId)
    {
        $leads['request']['leads']['update'] = [
            [
                'id'            => $leadId,
                'last_modified' => time(),
                'status_id'     => $statusId
            ]
        ];

        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/leads/set', $leads);
    }

    /**
     * @param $name
     * @param $leadId
     * @param $email
     * @param $phone
     * @param $task
     * @return mixed
     */
    public function storeContact($name, $leadId, $email, $phone)
    {
        $contacts['request']['contacts']['add'] = [
            [
                'name'            => $name,
                'linked_leads_id' => [
                    $leadId
                ],
                'custom_fields'   => [
                    [
                        // Phones
                        'id'     => 389343,
                        'values' => [
                            [
                                'value' => $phone,
                                'enum'  => 'MOB',
                            ],
                        ],
                    ],
                    [
                        //Emails
                        'id'     => 389345,
                        'values' => [
                            [
                                'value' => $email,
                                'enum'  => 'PRIV',
                            ],
                        ],
                    ]
                ]
            ],
        ];

        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/contacts/set', $contacts);
    }

}
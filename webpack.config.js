'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const SvgSpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const generalCssExtract = new ExtractTextPlugin('assets/css/style.css');

const extractUse = [
    {
        loader: "css-loader",
        options: {
            minimize: true,
            url: false
        }
    },
    {
        loader: 'postcss-loader',
        options: {
            ident: 'postcss',
            plugins: [
                require('autoprefixer')({
                    browsers:['ie >= 8', 'last 12 version']
                }),
            ]
        }
    },
    {
        loader: "sass-loader"
    }
];

const config = {
    entry: {
        general: ['babel-polyfill', './resources/bundles/general.js']
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'assets/js/[name].js',
        publicPath: ""
    },
    module: {
        rules: [
            {
                test: /style.scss/,
                use: generalCssExtract.extract({
                    fallback: 'style-loader',
                    use: extractUse
                })
            },
            {
                test: /(\.html$|\.phtml$|\.php)/,
                use: [
                    {
                        loader: 'ejs-loader'
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets": ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    devServer: {
        host: 'localhost',
        port: 4200
    },
    // watcher
    watchOptions: {
        aggregateTimeout: 100
    },
    plugins: [
        new CopyWebpackPlugin([
            {from:'resources/assets/videos',to:'assets/videos'},
            {from:'resources/assets/images',to:'assets/images'},
            {from:'resources/assets/fonts',to:'assets/fonts'},
        ]),
        new UglifyJsPlugin(),
        generalCssExtract,
        new SvgSpriteLoaderPlugin({
            plainSprite: true
        })
    ]
};

module.exports = config;

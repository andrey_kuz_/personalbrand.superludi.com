import $ from 'jquery';
import Reveal from 'foundation-sites';

$(document).ready(function() {

    function AutoShowPopup() {

        if (typeof window.location_hash === 'undefined' || !window.location_hash.length || !$(window.location_hash).length) {
            return;
        }

        setTimeout(function () {
            $(window.location_hash).foundation('open');
        }, 500);
    }

    AutoShowPopup();


});




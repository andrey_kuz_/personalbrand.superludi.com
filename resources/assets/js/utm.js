import $ from 'jquery';

$(document).ready(function() {

    //cooke
    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
        return vars;
    }

    //Get params
    function CatchUTM() {
        var url_string = window.location.href;
        var url = new URL(url_string);
        if (url.searchParams !== undefined) {
            var utm_source = url.searchParams.get("utm_source"),
                utm_medium = url.searchParams.get("utm_medium"),
                utm_campaign = url.searchParams.get("utm_campaign"),
                utm_term = url.searchParams.get("utm_term"),
                utm_content = url.searchParams.get("utm_content");
        } else {
            var params = getUrlVars(),
                utm_source = params["utm_source"],
                utm_medium = params["utm_medium"],
                utm_campaign = params["utm_campaign"],
                utm_term = params["utm_term"],
                utm_content = params["utm_content"];
        }
        var dateCookie = new Date;
        dateCookie.setDate(dateCookie.getDate() + 1);

        if (utm_source) {
            setCookie('utm_source', utm_source, {expires: dateCookie.toUTCString()});
        }
        if (utm_medium) {
            setCookie('utm_medium', utm_medium, {expires: dateCookie.toUTCString()});
        }
        if (utm_campaign) {
            setCookie('utm_campaign', utm_campaign, {expires: dateCookie.toUTCString()});
        }
        if (utm_term) {
            setCookie('utm_term', utm_term, {expires: dateCookie.toUTCString()});
        }
        if (utm_content) {
            setCookie('utm_content', utm_content, {expires: dateCookie.toUTCString()});
        }
    }

    CatchUTM();
});
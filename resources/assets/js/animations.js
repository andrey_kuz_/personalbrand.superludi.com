import $ from 'jquery';

$(document).ready(function() {

    const $appearBlocks = $('.animation.onscroll');

    const checkScroll = function() {

        const scrollTop = $(window).scrollTop() + $(this).height();

        $appearBlocks.each(function(i, o) {

            const blockPos = $(o).offset().top + ($(o).height() / 2);

            if(scrollTop >= blockPos){
                $(o).addClass('shown');
            }
        });
    };

    $(window).scroll(checkScroll);

    setTimeout(() => {
        checkScroll();
    }, 400);
});

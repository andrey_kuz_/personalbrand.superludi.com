import $ from 'jquery';
import Player from '@vimeo/player';

$(document).ready(function () {

    $(document).on('open.zf.reveal', function(event) {

        let video_iframe = $(event.target).find('iframe').get(0);

        if (video_iframe) {
            let player = new Player(video_iframe);
            player.play();
        }
    });

    $(document).on('closed.zf.reveal', function(event) {

        let video_iframe = $(event.target).find('iframe').get(0);

        if (video_iframe) {
            let player = new Player(video_iframe);
            player.pause();
        }
    });

    function saveVideoId() {
        document.cookie = "video_id=" + $(".videos__video.in").attr('id');
    }

    function getVideoId() {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + "video_id".replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));

        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    saveVideoId();

    function save_point(video) {
         var time = 0;
        if (getVideoId()) {
            time = Math.round(video.time());
            if (time) {
                $.ajax({
                    type: "POST",
                    url: "/video_point",
                    data: "time=" + time + "&video_id=" + getVideoId(),
                    success: function (msg) {
                        saveVideoId();
                    }
                });
            }
        }
    }

    $('.videos__list .videos__preview-wrap .videoframe a').click(function () {

        $('.videos__list .videos__preview-wrap .videoframe a').each(function () {
            $(this).removeClass("selected");
        });
        $(this).addClass("selected");

    });

    window._wq = window._wq || [];
    _wq.push({
        id: "_all", onReady: function (video) {
            var iframe;
            var time = $(".videos__video.in").attr('video-point');
            video.time(time);
            video.unmute();
            iframe = document.querySelector('.videos__video.fade.in iframe');
            video.bind("pause", function () {
                save_point(video);
            });
        }
    });

    $('.videos__preview-wrap').click(function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

});
import $ from 'jquery';

$(document).ready(function() {

    var current_video_id = '';

    function IsJsonString(str) {
        try {
            return typeof str == 'object';
        } catch (e) {
            return false;
        }
        return true;
    }

    function SimulationClick(link) {

        if (typeof link !== 'object') {
            return;
        }

        var linkEvent = null;
        if (document.createEvent) {
            linkEvent = document.createEvent('MouseEvents');
            linkEvent.initEvent('click', true, true);
            link.dispatchEvent(linkEvent);
        }
        else if (document.createEventObject) {
            linkEvent = document.createEventObject();
            link.fireEvent('onclick', linkEvent);
        }
    }

    function serverMessageBoxClear(server_message_box) {

        if (typeof server_message_box !== 'object') {
            return;
        }

        server_message_box.hide();
        server_message_box.html('');
        server_message_box.removeClass('error');
    }

    function OnPay(user_id, video_id) {

        var server_message_box = $("#modal-buy-course-second-step").find('.server-message-box');
        serverMessageBoxClear(server_message_box);

        $('#modal-buy-course').find('.modal-close.close').trigger("click");
        SimulationClick($('#modal_buy_course_second_step_show')[0]);

        $.ajax({
            type: 'POST',
            url: $("#checkout_step_first").attr('action-paying'),
            data: {user_id: user_id, video_id: video_id},
            dataType: 'json',
            success: function (data) {

                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    $('#modal-buy-course-second-step').find('.js-message-pay-in-progress').hide();
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else if (data.success && 'form' in data && SetPayForm(data.form)) {
                    $('#pay_form').submit();
                }

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    function ValidPayForm(data) {
        return typeof data === 'object' &&
            'action' in data && data.action.length &&
            'key' in data && data.key.length &&
            'payment' in data && data.payment.length &&
            'order' in data && data.order.length &&
            'data' in data && data.data.length &&
            'sign' in data && data.sign.length
            ;
    }

    function SetPayForm(data) {

        if (!$('#pay_form').length || !ValidPayForm(data) || !$('#pay_form')[0].hasAttribute('action')) {
            console.log(data);
            console.log($('#pay_form').length);
            console.log(ValidPayForm(data));
            console.log($('#pay_form')[0].hasAttribute('action'));
            return false;
        }

        $('#pay_form').attr('action', data.action);

        if ($('#pay_form').find('input[name="key"]').length) {
            $('#pay_form').find('input[name="key"]').val(data.key);
        }
        if ($('#pay_form').find('input[name="payment"]').length) {
            $('#pay_form').find('input[name="payment"]').val(data.payment);
        }
        if ($('#pay_form').find('input[name="order"]').length) {
            $('#pay_form').find('input[name="order"]').val(data.order);
        }
        if ($('#pay_form').find('input[name="data"]').length) {
            $('#pay_form').find('input[name="data"]').val(data.data);
        }
        if ($('#pay_form').find('input[name="sign"]').length) {
            $('#pay_form').find('input[name="sign"]').val(data.sign);
        }
        if ($('#pay_form').find('input[name="ext1"]').length) {
            $('#pay_form').find('input[name="ext1"]').val(data.ext1);
        }

        return true;
    }

    $('[data-target="#modal-buy-course"], [data-target="#modal-buy-course-second-step"]').on('click', function (e) {

        if ($(this)[0].hasAttribute('course-all')) {
            current_video_id = $(this).attr('course-all');
        }
    });


    $('[data-target="#modal-buy-course-second-step"]').on('click', function (e) {

        if ($(this)[0].hasAttribute('id') && $(this).attr('id') == 'modal_buy_course_second_step_show') {
            return;
        }

        var server_message_box = $('#modal-buy-course-second-step').find('.server-message-box');
        serverMessageBoxClear(server_message_box);

        $('#modal-buy-course').find('.modal-close.close').trigger("click");
        SimulationClick($('#modal_buy_course_second_step_show')[0]);

        $.ajax({
            type: 'POST',
            url: $("#checkout_step_first").attr('action-paying'),
            data: {video_id: current_video_id},
            dataType: 'json',
            success: function (data) {
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    $('#modal-buy-course-second-step').find('.js-message-pay-in-progress').hide();
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else if (data.success && 'form' in data && SetPayForm(data.form)) {
                    $('#pay_form').submit();
                }

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


    $("#checkout_step_first").on('submit', function (e) {
        $('#user_agree_checkout_step_first_div').removeClass('has-error');
        e.preventDefault();
        if ($("#user_agree_checkout_step_first").is(":checked")) {
            var server_message_box;

            if ($(this).find('.server-message-box').length) {
                server_message_box = $(this).find('.server-message-box');
                serverMessageBoxClear(server_message_box);
            }

            var user_name = $(this).find('input[name="user_name"]').val();
            var user_phone = $(this).find('input[name="user_phone"]').val();
            var user_email = $(this).find('input[name="user_email"]').val();

            if (!user_name.length) {
                $(this).find('input[name="user_name"]').parent().addClass('has-error');
            }
            else {
                $(this).find('input[name="user_name"]').parent().removeClass('has-error');
            }

            if (!user_phone.length) {
                $(this).find('input[name="user_phone"]').parent().parent().parent().addClass('has-error');
            }
            else {
                $(this).find('input[name="user_phone"]').parent().parent().parent().removeClass('has-error');
            }

            if (!user_email.length) {
                $(this).find('input[name="user_email"]').parent().addClass('has-error');
            }
            else {
                $(this).find('input[name="user_email"]').parent().removeClass('has-error');
            }

            if ($(this).find('.has-error').length > 0) {
                return;
            }

            if (!user_name.length || !user_phone.length || !user_email.length) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: $("#checkout_step_first").attr('action'),
                data: {name: user_name, phone: user_phone, email: user_email},
                dataType: 'json',
                success: function (data) {
                    if (!IsJsonString(data)) {
                        return;
                    }

                    if (!data.success) {
                        server_message_box.addClass('error');
                    }
                    else {
                        $("#checkout_step_first").trigger("reset");

                        setTimeout(function () {
                            serverMessageBoxClear(server_message_box);
                            OnPay(data.user_id, current_video_id);
                        }, 2000);
                    }

                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });


            return false;
        } else {
            $('#user_agree_checkout_step_first_div').addClass('has-error');
            return false;
        }
    });

    function AutoShowPopup() {

        if (typeof window.location_hash === 'undefined' || !window.location_hash.length || !$('[data-target="' + window.location_hash + '"]').length) {
            return;
        }

        setTimeout(function () {
            $(window.location_hash).foundation('open');
            if (window.location_hash == "#modal-buy-course-3") {
                dataLayer.push({event: 'gtm.dom', uniqueEventId: success});
            }
            // SimulationClick($('[data-target="' + window.location_hash + '"]')[0]);
        }, 500);
    }

    AutoShowPopup();

    function AutoShowOrderToPay() {
        if (typeof window.order_to_pay === 'undefined' || !window.order_to_pay.length || !$('#' + window.order_to_pay).length) {
            return;
        }

        setTimeout(function () {
            SimulationClick($('#' + window.order_to_pay)[0]);
        }, 500);
    }
    AutoShowOrderToPay();

    function RedirectToPurchasedVideo() {
        if (!('url_purchased_video' in window) || !window.url_purchased_video.length) {
            return;
        }

        setTimeout(function () {
            window.location.href = window.url_purchased_video;
        }, 2000);
    }

    RedirectToPurchasedVideo();

});
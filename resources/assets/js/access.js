import $ from 'jquery';

$(document).ready(function () {

    function accessCheck() {
        $.ajax({
            type: "post",
            url: "/access",
            success: function (data) {
                if (data == 'banned') {
                    window.location.href = location.pathname;
                }
            }
        });
    }

    accessCheck();
    setInterval(accessCheck, 15000);

});
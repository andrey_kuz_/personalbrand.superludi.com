import $ from 'jquery';
import Reveal from 'foundation-sites';

$(document).ready(function() {

    const $body = $('body');

    $body.on('click', '[data-action=scrollTo]', function() {

        let $target = $($(this).attr('data-target'));

        if ($target.length) {
            $target.get(0).scrollIntoView({block: 'start', behavior: 'smooth'})
            return false;
        } else {
            return true;
        }
    });

    $body.on('click', '[data-action=openModal]', function() {

        $($(this).attr('data-target')).foundation('open');

        return false;
    });

    $body.on('click', '[data-action=toggleActive]', function() {

        let $item;

        if ($(this).attr('data-target')) {
            $item = $($(this).attr('data-target'));
        } else {
            $item = $(this);
        }

        $item.toggleClass('active');
    });

    $body.on('click', '[data-action=toggleSlide]', function() {

        let $item;

        if ($(this).attr('data-target')) {
            $item = $($(this).attr('data-target'));
        } else {
            $item = $(this);
        }

        $item.slideToggle();

        return false;
    });

    $body.on('click', '[data-action=fadeIn]', function() {

        let $item;

        if ($(this).attr('data-target')) {
            $item = $($(this).attr('data-target'));
        } else {
            $item = $(this);
        }

        $item.siblings('.fade').removeClass('in');
        $item.addClass('in');

        return false;
    });
});

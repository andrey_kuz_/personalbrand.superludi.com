import $ from 'jquery';

$('.hypercomments_comment_div').bind("DOMSubtreeModified", function () {
    var image_form =  $('.hcc.hc__form .hcc.hc__reply__photo img').attr('src');
    if(typeof image_form !== 'undefined') {
        if (image_form.search('static.hypercomments.com/data/avatars/0/avatar') != -1) {
            $('.hcc.hc__reply__photo img').replaceWith('<svg xmlns="http://www.w3.org/2000/svg" width="400" height="400" viewBox="0 0 400 400" fill="none"><rect width="400" height="400" fill="white"/> <rect width="365" height="365" fill="black" fill-opacity="0" transform="translate(17 18)"/> <circle cx="199.5" cy="200.5" r="182.5" fill="#C9002C"/> <path d="M207.254 316.636C143.506 316.636 91.5267 264.73 91.5267 201.072C91.5267 137.414 143.506 85.5078 207.254 85.5078C239.619 85.5078 269.041 98.2395 290.618 121.744L261.195 150.145C246.484 135.455 227.85 126.641 207.254 126.641C166.063 126.641 132.718 159.939 132.718 201.072C132.718 242.205 166.063 275.503 207.254 275.503C227.85 275.503 247.465 267.668 261.195 251.999L290.618 280.4C269.041 303.905 239.619 316.636 207.254 316.636Z" fill="white"/> </svg>');
        }
    }
    $(".hcc.hc__message__header span").each(function () {
        $(this).attr('data-href','');
    });

    $(".hcc.hc__avatar img").each(function (index) {
        var text = $(this).attr('src');
        $(this).attr('data-href','');
        if (text.search('static.hypercomments.com/data/avatars/0/avatar') != -1) {
            $(this).replaceWith('<svg xmlns="http://www.w3.org/2000/svg" width="400" height="400" viewBox="0 0 400 400" fill="none"><rect width="400" height="400" fill="white"/> <rect width="365" height="365" fill="black" fill-opacity="0" transform="translate(17 18)"/> <circle cx="199.5" cy="200.5" r="182.5" fill="#C9002C"/> <path d="M207.254 316.636C143.506 316.636 91.5267 264.73 91.5267 201.072C91.5267 137.414 143.506 85.5078 207.254 85.5078C239.619 85.5078 269.041 98.2395 290.618 121.744L261.195 150.145C246.484 135.455 227.85 126.641 207.254 126.641C166.063 126.641 132.718 159.939 132.718 201.072C132.718 242.205 166.063 275.503 207.254 275.503C227.85 275.503 247.465 267.668 261.195 251.999L290.618 280.4C269.041 303.905 239.619 316.636 207.254 316.636Z" fill="white"/> </svg>');
        }
    });

});
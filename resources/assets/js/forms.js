import $ from 'jquery';
import 'intl-tel-input';
//import 'intl-tel-input/src/js/utils';
import './utils';

window.jQuery = $;

$(document).ready(function () {

    $('input[type=tel]').each(function (i, a) {
        $(a).intlTelInput({
            preferredCountries: ['ua', 'ru'],
            nationalMode: true,
            separateDialCode: false,
            formatOnDisplay: true,
            autoPlaceholder: "polite",
            numberType: "MOBILE"
        });
    });

    function add_plus(input) {
        var text = input.val();
        if (typeof input.intlTelInput("getSelectedCountryData")['dialCode'] != "undefined") {
            if (!(text.indexOf('+') + 1)) {
                input.val(text);
            }
        } else {
            if (!(text.indexOf('+') + 1) && (text.length > 0)) {
                var addplus = '+' + text;
                input.val(addplus);
            }
        }
    }


    $("body").on("click touchstart", ".intl-tel-input.allow-dropdown .flag-container", function () {
        if ($(this).closest(".intl-tel-input.allow-dropdown").find('input').intlTelInput("getSelectedCountryData") != "undefined") {
            var target = $(this);
            var code = "+" + target.closest(".intl-tel-input.allow-dropdown").find('input').intlTelInput("getSelectedCountryData")['dialCode'];
            var text = target.closest(".intl-tel-input.allow-dropdown").find('input').val();
            var start = text.search(' ');
            text = text.substring(start);
            text = code + text;

            if (typeof code != "undefined" && code != "undefined") {
                target.closest(".intl-tel-input.allow-dropdown").find('input').val();
            }
        }
    });

    $("body").on("keypress", ".intl-tel-input.allow-dropdown input", function (e) {
        var symbolsMap = {
            "A": "2",
            "B": "2",
            "C": "2",
            "D": "3",
            "E": "3",
            "F": "3",
            "G": "4",
            "H": "4",
            "I": "4",
            "J": "5",
            "K": "5",
            "L": "5",
            "M": "6",
            "N": "6",
            "O": "6",
            "P": "7",
            "Q": "7",
            "R": "7",
            "S": "7",
            "T": "8",
            "U": "8",
            "V": "8",
            "W": "9",
            "X": "9",
            "Y": "9",
            "Z": "9"
        };
        if (typeof $(this).intlTelInput("getSelectedCountryData")['dialCode'] != "undefined") {
            if (e.keyCode == 32) {
                return false;
            }

            var before_val = $(this).val();
            var after_val = '';
            var currentCountry = $(this).intlTelInput("getSelectedCountryData");
            for (var i = 0; i < before_val.length; i++) {
                if (!symbolsMap.hasOwnProperty(before_val[i].toUpperCase())) {
                    after_val += before_val[i];
                }
                if (symbolsMap.hasOwnProperty(before_val[i].toUpperCase())) {
                    after_val += symbolsMap[before_val[i].toUpperCase()];
                }
                if ((i == ($(this).intlTelInput("getSelectedCountryData")['dialCode'].length + 1))) {
                    if (before_val[i] != ' ') {
                        after_val += ' ';

                    }
                }


            }
            var text_space_count = (after_val.split(" ").length - 1);

            if (currentCountry.iso2 === "ua" && text_space_count < 2) {
                after_val = after_val.substring(0, 13);
            }


            if (currentCountry.iso2 === "ru" && text_space_count < 2) {
                after_val = after_val.substring(0, 13);
            }

            $(this).val(after_val);
            var text = $(this).val();

        }

    });

    $("body").on("keydown", ".intl-tel-input.allow-dropdown input", function (e) {
        if (e.keyCode == 32) {
            return false;
        }
        add_plus($(this));


        if (typeof $(this).intlTelInput("getSelectedCountryData")['dialCode'] != "undefined") {
            var text = $(this).val();
            var text_space_count = (text.split(" ").length - 1);
            if (text_space_count == 0 && (text.length == $(this).intlTelInput("getSelectedCountryData")['dialCode'].length + 2)) {
                var part = text.substr($(this).intlTelInput("getSelectedCountryData")['dialCode'].length + 1, text.length);
                var code = text.substr(0, $(this).intlTelInput("getSelectedCountryData")['dialCode'].length + 1);
                if(part != "") {
                    $(this).val(code + ' ' + part);
                }
            }
        }


    });

    $("body").on("keyup", ".intl-tel-input.allow-dropdown input", function (e) {
        if (e.keyCode == 32) {
            return false;
        }



        if (typeof $(this).intlTelInput("getSelectedCountryData") != "undefined") {
            add_plus($(this));
            var text = $(this).val();
            var correct_code = text.split(" ");
            var must_be = '+' + $(this).intlTelInput("getSelectedCountryData")['dialCode'];
            if (correct_code[0] != must_be && typeof correct_code[1] != "undefined") {
                var correct = correct_code[0].substr(0, must_be.length);
                var another_part = correct_code[0].substr(must_be.length, correct_code[0].length);
                correct_code[0] = correct;
                correct_code[1] = another_part + correct_code[1];
                $(this).val(correct_code.join(' '));
            }
        }

        var text_space_count = (text.split(" ").length - 1);
        if (typeof $(this).intlTelInput("getSelectedCountryData")['dialCode'] != "undefined") {
            if ($(this).intlTelInput("getSelectedCountryData").iso2 !== "ua" && (e.keyCode != 37 || e.keyCode != 39)) {

                if (  text_space_count > 1) {
                    var data = text.split(" ");
                    var text_start = $(this).intlTelInput("getSelectedCountryData")['dialCode'];
                    text = text.replace(/\s+/g, '');
                    text_start = '+'+$(this).intlTelInput("getSelectedCountryData")['dialCode'];
                    text = text.substr(data[0].length);

                    $(this).val(text_start +' '+ text);


                }
            }
        }

        var in_valid = true;

        if (((e.keyCode == 187 && e.shiftKey === true) && $(this).val() == ('+' + $(this).intlTelInput("getSelectedCountryData")['dialCode'] + ' +') ) || (e.keyCode == 107 && ($(this).val() == '+' + $(this).intlTelInput("getSelectedCountryData")['dialCode'] + ' +'))) {
            $(this).val('+');
        }

        if (e.keyCode == 144 || e.keyCode == 46 || e.keyCode == 18 || e.keyCode == 91 || e.keyCode == 92 || e.keyCode == 13 || e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 || e.keyCode == 42 || e.keyCode == 40 || e.keyCode == 145 || e.keyCode == 19 || e.keyCode == 17 || e.keyCode == 16 || e.keyCode == 20 ||
            (e.keyCode == 33 || e.keyCode == 34) ||
            (e.keyCode == 187 && e.shiftKey === true) ||
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode == 67 && e.ctrlKey === true) ||
            (e.keyCode == 86 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39) || (e.keyCode >= 112 && e.keyCode <= 123)) {
            in_valid = false;
        }

        if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 ) && e.keyCode != 107 && in_valid) {

            var text_space = $(this).val();
            if (text_space.indexOf(e.key) == text_space.length) {
                $(this).val(text_space.substring(0, (text_space.length - 1)));
            } else {
                var start = text_space.substring(0, text_space.indexOf(e.key));
                var end = text_space.substring(text_space.indexOf(e.key) + 1, text_space.length);
                $(this).val(start + end);
            }
        }



        if (typeof $(this).intlTelInput("getSelectedCountryData")['dialCode'] != "undefined") {

            if (((e.keyCode == 187 && e.shiftKey === true) && $(this).val().length > $(this).intlTelInput("getSelectedCountryData")['dialCode'].length ) || (e.keyCode == 107 && $(this).val().length > $(this).intlTelInput("getSelectedCountryData")['dialCode'].length )) {
                var text_space = $(this).val();
                if (text_space[text_space.length - 1] == '+') {
                    $(this).val(text_space.substring(0, (text_space.length - 1)));
                }
            }
        }

        var target = $(this);

        if (typeof target.intlTelInput("getSelectedCountryData")['dialCode'] != "undefined") {
            var code = "+" + target.intlTelInput("getSelectedCountryData")['dialCode'];
        }


        if (typeof code != "undefined" && code != "undefined" && typeof target.intlTelInput("getSelectedCountryData")['dialCode'] != "undefined") {
            if (target.val().indexOf("+" + target.intlTelInput("getSelectedCountryData")['dialCode']) != -1) {
                target.val(target.val().substr(target.val().indexOf("+" + target.intlTelInput("getSelectedCountryData")['dialCode'])));
            }
            if (!target.val()) {

            } else if (target.val() == "+" + target.intlTelInput("getSelectedCountryData")['dialCode'] && e.keyCode != 8) {
                target.val(target.val() + " ");

            }

        }
        var text = target.val();
        if (typeof target.intlTelInput("getSelectedCountryData") != "undefined") {

            if (typeof $(this).intlTelInput("getSelectedCountryData").dialCode != "undefined") {
                if ($(this).intlTelInput("getSelectedCountryData").dialCode.slice(-1) == 0) {
                    var text = $(this).val();
                    var parts_text = text.split(" ");

                    $(this).val(parts_text.join(' '));
                    var parts_text = text.split(" ");
                    if (parts_text.length > 3 && text.length > 14) {

                        if (parts_text[1][0] == 0) {
                            parts_text[1] = parts_text[1].substr(1) + parts_text[2][0];
                            parts_text[2] = parts_text[2].substr(1) + parts_text[3][0];
                            parts_text[3] = parts_text[3].substr(1);
                            text = parts_text.join(' ');
                            $(this).val(text);
                        }

                    }
                    if (parts_text.length == 3 && text.length > 14) {
                        if (parts_text[1][0] == 0) {
                            parts_text[1] = parts_text[1].substr(1) + parts_text[2][0];
                            parts_text[2] = parts_text[2].substr(1);
                            text = parts_text.join(' ');
                            $(this).val(text);
                        }

                    }
                    if (parts_text.length == 2 && text.length > 14) {
                        if (parts_text[1][0] == 0) {
                            parts_text[1] = parts_text[1].substr(1);
                            text = parts_text.join(' ');
                            $(this).val(text);
                        }

                    }

                }
            }

            text = target.val();
            var currentCountry = target.intlTelInput("getSelectedCountryData");
            var text_space_count = (text.split(" ").length - 1);
            if (text_space_count > 1) {
                if (currentCountry.iso2 === "ua") {
                    if (text_space_count == 2) {

                        var update_text = text.split(" ");
                        var part_text = update_text[2];
                        var update_text_part_first = "";
                        var update_text_part = "";
                        var update_text_part1 = "";

                        if (update_text[0].length > currentCountry.dialCode.length + 1) {
                            for (var i = 0; i < update_text[0].length; i++) {
                                update_text_part_first += update_text[0][i];
                                if (i == currentCountry.dialCode.length) {
                                    update_text_part_first += ' ';
                                }
                            }
                        }

                        if (update_text[1].length == 5) {
                            for (var i = 0; i < update_text[1].length; i++) {
                                update_text_part1 += update_text[1][i];
                                if (i == 1) {
                                    update_text_part1 += ' ';
                                }
                            }
                        }

                        for (var i = 0; i < part_text.length; i++) {
                            update_text_part += part_text[i];
                            if (i == 2 && part_text.length == 7) {
                                update_text_part += ' ';
                            }
                        }

                        if (update_text_part_first == '') {
                            update_text_part_first = update_text[0];
                        }

                        if (update_text_part1 == '') {
                            update_text_part1 = update_text[1];
                        }

                        text = update_text_part_first + ' ' + update_text_part1 + ' ' + update_text_part;
                        text = text.substring(0, 16);
                        target.val(text);
                    }
                }

            }


            var text = target.val();
            var space_pos = text.indexOf(" ");
            var text_countryCode = text.substring(0, space_pos);
            var other_part = text.substring(space_pos);
            var VRegExp = new RegExp(/^[ ]+/g);
            other_part = other_part.replace(VRegExp, '');
            if (text_countryCode && other_part) {
                text = text_countryCode + ' ' + other_part;
            }
            var currentCountry = target.intlTelInput("getSelectedCountryData");


        }

        var text_space_count = (text.split(" ").length - 1);
        if (text_space_count == 0 && (text.length > 2 && text[0] != '+' ) && (typeof currentCountry.dialCode != "undefined")) {
            text = '+' + currentCountry.dialCode + ' ' + text;
        }

        if (typeof target.intlTelInput("getSelectedCountryData").iso2 != "undefined" ) {

            if (currentCountry.iso2 === "ua") {
                text = text.substring(0, 16);
            }
            if (currentCountry.iso2 === "ru") {
                text = text.substring(0, 13);
            }

        }

        if ((currentCountry.iso2 === "ua" && text.length == 14 && text_space_count < 2) || (currentCountry.iso2 === "ua" && text.length > 14 && text_space_count < 3)) {

            var space_pos = text.indexOf(" ");
            var first_part = text.substring(0, space_pos);

            var second_part = text.substring(space_pos, space_pos + 3);
            second_part += " ";
            var third_part = text.substring(space_pos + 3, space_pos + 6)+' ';
            var other_part = text.substring(space_pos + 6,space_pos + 11);
            text = first_part + second_part + third_part + other_part;
            target.val(text.substring(0, 16));
        }

        else {
            target.val(text);
        }

    });

    $("body").on("focus  touchstart click", ".intl-tel-input.allow-dropdown input", function (e) {
        var target = $(this);
        if (target.val() == '') {
            var code = "+" + target.intlTelInput("getSelectedCountryData")['dialCode'];
            code = code + target.val();

            if (typeof target.intlTelInput("getSelectedCountryData") == "undefined") {
                code = "+" + target.intlTelInput("getSelectedCountryData")['dialCode'];
            }

            if (typeof target.intlTelInput("getSelectedCountryData") != "undefined") {
                if (!target.val()) {
                    target.val(code);
                }
                if (target.setSelectionRange) {
                    // ... then use it (Doesn't work in IE)

                    // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
                    var len = target.val().length;

                    target.setSelectionRange(len, len);

                } else {
                    // ... otherwise replace the contents with itself
                    // (Doesn't work in Google Chrome)

                    target.val($(this).val());

                }

            }
        }
    });

});
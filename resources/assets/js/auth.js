import $ from 'jquery';

$(document).ready(function () {

    function IsJsonString(str) {
        try {
            return typeof str == 'object';
        } catch (e) {
            return false;
        }
        return true;
    }

    function serverMessageBoxClear(server_message_box) {
        if (typeof server_message_box !== 'object') {
            return;
        }

        server_message_box.hide();
        server_message_box.html('');
        server_message_box.removeClass('error');
    }

    $('#login_form').bind('submit', function (e) {
        e.preventDefault();
        var server_message_box;
        if ($(this).find('.server-message-box').length) {
            server_message_box = $(this).find('.server-message-box');
            serverMessageBoxClear(server_message_box);
        }

        var user_email = $(this).find('input[name="user_email"]').val();
        var user_password = $(this).find('input[name="user_password"]').val();

        if (!user_email.length) {
            $(this).find('input[name="user_email"]').parent().addClass('has-error');
        }
        else {
            $(this).find('input[name="user_email"]').parent().removeClass('has-error');
        }

        if (!user_password.length) {
            $(this).find('input[name="user_password"]').parent().addClass('has-error');
        } else {
            $(this).find('input[name="user_password"]').parent().removeClass('has-error');
        }

        if ($(this).find('.has-error').length > 0) {
            return;
        }

        if (!user_email.length || !user_password.length) {
            return;
        }

        $.ajax({
            type: 'POST',
            beforeSend: function (request) {
                request.setRequestHeader("Access-Control-Allow-Origin", document.location.origin);
            },
            url: $('#login_form').attr('action'),
            data: {email: user_email, password: user_password},
            dataType: 'json',
            success: function (data) {

                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    server_message_box.addClass('error');

                }
                else {
                    $('#login_form').trigger("reset");
                    setTimeout(function () {
                        serverMessageBoxClear(server_message_box);
                        $('#login_form').find('.modal-close.close').trigger("click");
                        window.location.href = data.redirect;
                    }, 1500);
                }
                server_message_box.html(data.message);
                server_message_box.fadeIn('slow');

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


    $('#form_request_phone').bind('submit', function (e) {
        e.preventDefault();
        var server_message_box;
        if ($("#form_request_phone").find('.server-message-box').length) {
            server_message_box = $("#form_request_phone").find('.server-message-box');
            serverMessageBoxClear(server_message_box);
        }

        var user_phone = $(this).find('input[name="user_phone"]').val();

        if (!user_phone.length) {
            $(this).find('input[name="user_phone"]').parent().addClass('has-error');
        }
        else {
            $(this).find('input[name="user_phone"]').parent().removeClass('has-error');
        }

        if (!user_phone.length) {
            return;
        }

        if ($("#form_request_phone").find('.has-error').length > 0) {
            return;
        }

        $("#form_request_phone_button").addClass('not-active');
        var data = {
            name: window.request_phone.name,
            email: window.request_phone.email,
            promocode: window.request_phone.promocode,
            phone: user_phone,
            is_auto_login: 1
        };

        if ('password' in window.request_phone) {
            data.password = window.request_phone.password;
        }
        $.ajax({
            type: 'POST',
            url: $("#form_request_phone").attr('action'),
            data: data,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                
                $("#form_request_phone_button").removeClass('not-active');
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else if (data.success) {
                    $('#form_request_phone').trigger("reset");
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                    setTimeout(function () {
                        window.location.href = window.request_phone.redirect;
                    }, 1500);
                }
            },
            error: function (data) {
                $("#form_request_phone_button").removeClass('not-active');
                console.log('Error:', data);
            }
        });
    });

    /*$("#form_registration #user_agree").change(function() {
        if(this.checked) {
            $('#form_registration_button').prop( "disabled", false );
        } else {
            $('#form_registration_button').prop( "disabled", true );
        }
    });*/


    $('#form_registration').bind('submit', function (e) {
        e.preventDefault();
        $('#user_agree_div').removeClass('has-error');
        if ($("#form_registration").find('#user_agree').is(":checked")) {

            var server_message_box;
            if ($("#form_registration").find('.server-message-box').length) {
                server_message_box = $("#form_registration").find('.server-message-box');
                serverMessageBoxClear(server_message_box);
            }

            var user_email = $(this).find('input[name="user_email"]').val();

            var user_password = $(this).find('input[name="user_password"]').val();

            if (!user_email.length) {
                $(this).find('input[name="user_email"]').parent().addClass('has-error');
            }
            else {
                $(this).find('input[name="user_email"]').parent().removeClass('has-error');
            }

            if (!user_password.length) {
                $(this).find('input[name="user_password"]').parent().addClass('has-error');
            }
            else {
                $(this).find('input[name="user_password"]').parent().removeClass('has-error');
            }

            if (!user_email.length || !user_password.length) {
                return;
            }

            if ($("#form_registration").find('.has-error').length > 0) {
                return;
            }

            var promocode = $(this).find('input[name="promocode"]').val();
            $("#form_registration_button").addClass('not-active');

            $.ajax({
                type: 'POST',
                url: $("#form_registration").attr('action'),
                data: {email: user_email, password: user_password, promocode: promocode },
                dataType: 'json',
                success: function (data) {
                    $("#form_registration_button").removeClass('not-active');
                    if (!IsJsonString(data)) {
                        return;
                    }

                    if (!data.success) {
                        server_message_box.addClass('error');
                        server_message_box.html(data.message);
                        server_message_box.fadeIn('slow');
                    }

                    else if (data.success) {

                        $('#form_registration').trigger("reset");
                        $('#modal-registration').find('.modal-close.close').trigger("click");
                        window.request_phone = data.user_info;
                        $('#modal_phone_user_label').text(window.request_phone.name);
                        $('#modal-phone').foundation('open');
                    }
                },
                error: function (data) {
                    $("#form_registration_button").removeClass('not-active');
                    console.log('Error:', data);
                }
            });
        } else {
            $('#user_agree_div').addClass('has-error');
            return false;
        }

    });

    $('.format__content .format__content-btn .button.btn-buy').click(function () {
        let value = $(this).closest('.format__content').find('.format__content-title h3').text().toLowerCase();
        document.cookie ='buyPack='+value ;
    });

    $('.format__content .format__content-btn .button.btn-free').click(function () {
        let value = $(this).closest('.format__content').find('.format__content-title h3').text().toLowerCase();
        document.cookie ='pack='+value ;
    });

    $("#form_pass_recovery").bind('submit', function (e) {
        e.preventDefault();
        var server_message_box;
        if ($(this).find('.server-message-box.form-pass-recovery').length) {
            server_message_box = $(this).find('.server-message-box.form-pass-recovery');
            serverMessageBoxClear(server_message_box);
        }

        var user_email = $(this).find('input[name="user_email"]').val();

        if (!user_email.length) {
            $(this).find('input[name="user_email"]').parent().addClass('has-error');
        }
        else {
            $(this).find('input[name="user_email"]').parent().removeClass('has-error');
        }

        if (!user_email.length) {
            return;
        }

        if ($(this).find('.has-error').length > 0) {
            return;
        }

        $.ajax({
            type: 'POST',
            url: $("#form_pass_recovery").attr('action'),
            data: {email: user_email},
            dataType: 'json',
            success: function (data) {
                if (!IsJsonString(data)) {
                    return;
                }

                if (!data.success) {
                    server_message_box.addClass('error');
                    server_message_box.html(data.message);
                    server_message_box.fadeIn('slow');
                }
                else {
                    $("#form_pass_recovery").trigger("reset");
                    $('#modal-pass-recovery').find('.modal-close.close').trigger("click");
                    $('#modal-pass-success').foundation('open');
                }

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });



});
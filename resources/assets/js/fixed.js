import $ from 'jquery';

$(document).ready(function() {

    let $header = $('header');

    $(window).scroll(function() {
        if ($(window).scrollTop() >= $(window).height()*0.25) {
            $header.addClass('fixed');
        } else {
            $header.removeClass('fixed');
        }
        if ($(window).scrollTop() >= $(window).height()*0.45) {
            $header.addClass('header_fixed header_transition header_show');
        } else {
            $header.removeClass('header_fixed header_transition header_show');
        }
    });
});

$(document).ready(function() {

    let $phone = $('body');

    $(window).scroll(function() {
        if ($(window).scrollTop() >= $(window).height()*0.45) {
            $phone.addClass('show__phone');
        } else {
            $phone.removeClass('show__phone');
        }
    });
});


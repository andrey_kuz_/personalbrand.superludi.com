import $ from 'jquery';

$(document).ready(function () {

   /* $("#user_agree_subscribe").change(function () {
        if (this.checked) {
            $('.subscribe-box__form #form_subscribe_button').prop("disabled", false);
        } else {
            $('.subscribe-box__form #form_subscribe_button').prop("disabled", true);
        }
    });*/

$('.subscribe-box__form').on('submit', function (e) {
    $('.user_agree_subscribe .error-message').hide();
    e.preventDefault();
    if ($("#user_agree_subscribe").is(":checked")) {
        $('.knowledge__searchForm .subscribe-box__form-message').html('');
        $.ajax({
            url: '/mail',
            type: "post",
            data: $(this).serialize(),
            success: function (data) {
                if (data == "Этот емейл уже подписан") {
                    $('.knowledge__searchForm .subscribe-box__form-message').html(data);
                }
                else {
                    $('.knowledge__searchForm__form').hide();

                    $('.knowledge__searchForm .subscribe-box__form-success').html(data);
                }
            },
            error: function (data) {
                var errors = data.responseJSON;
                $('.knowledge__searchForm .subscribe-box__form-message').html((errors && errors['email']) ? errors['email'][0] : 'Введите E-mail');
            }
        });
    } else {
        $('.user_agree_subscribe .error-message').show();
        return false;
    }


});
});
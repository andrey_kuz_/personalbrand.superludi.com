import 'slick-carousel';
import $ from 'jquery';

$(document).ready(function() {

    const opinionSlidesCount = $('.opinion__slider .opinion__slider-item').length;

    $('.opinion__slider.slider').slick({
        'arrows': true,
        'dots': false,
        'slidesToShow': 1,
        'speed': 600,
        'infinite': true,
        'swipeToSlide': true
        // 'responsive': [
        //     {
        //         'breakpoint': 768,
        //         'settings': {
        //             'adaptiveHeight': true
        //         }
        //     }
        // ]

    });

    $(".opinion__slider.slider").on('afterChange', function(event, slick, currentSlide){
        $('.opinion__slider-counter').html((currentSlide + 1) + '/' + opinionSlidesCount);
    });
});


// var $slider = $('.slider');

//
// if ($slider.length) {
//     var currentSlide;
//     var slidesCount;
//     var sliderCounter = document.createElement('div');
//     sliderCounter.classList.add('slider__counter');
//
//     var updateSliderCounter = function(slick, currentIndex) {
//         currentSlide = slick.slickCurrentSlide() + 1;
//         slidesCount = slick.slideCount;
//         $(sliderCounter).text(currentSlide + '/' +slidesCount)
//     };
//
//     $slider.on('init', function(event, slick) {
//         $slider.append(sliderCounter);
//         updateSliderCounter(slick);
//     });
//
//     $slider.on('afterChange', function(event, slick, currentSlide) {
//         updateSliderCounter(slick, currentSlide);
//     });
//
//     $slider.slick();
// }
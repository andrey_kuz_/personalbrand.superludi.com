<?php

return [
    'me_content_view' => 'Content page',

    // example

    'me_example_list' => 'Examples',

    'ps_example' => 'Examples',
    'p_example_list' => 'List examples',
    'p_example_edit' => 'Add \ Edit examples',
    'p_example_delete' => 'Delete examples',
    
    'm_example' => 'Examples',
    'mi_example_list' => 'Examples list',

    'ss_example' => 'Examples',
    's_text' => 'Text',

    // other
];
<?php

return [
    'error_create_order' => 'Ошибка создания заказа',
	'full_course' => 'Весь курс',
	'video' => 'видео',
	'package_name' => 'Персональный Пакет ":package_name"',
];
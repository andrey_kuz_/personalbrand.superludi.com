<?php

return [
	'phone' => 'Номер телефона',
    'delete_operation_denied' => 'У пользователя :name найдены заказ(ы) видео. В операции удаления отказано',
    'promocode' => 'Промокод'
];
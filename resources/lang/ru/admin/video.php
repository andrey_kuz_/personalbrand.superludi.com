<?php

return [
    'list' => 'Список видео',
    'add' => 'Добавить видео',
    'add_btn' => 'Добавить',
    'edit' => 'Редактировать видео',
    'sort' => 'Сортировать видео',
    'sort_btn' => 'Сортировка',
    'saved' => 'Видео сохранено',
    'sorted' => 'Видео отсортировано',

    'visible' => 'Видимость',
    'published' => 'Опубликован',
    'hidden' => 'Скрыт',

    'f_title' => 'Заголовок',
    'f_permalink' => 'ЧПУ',
    'f_published_at' => 'Дата публикации',
	'is_free_label' => 'Бесплатное',
	'is_free_placeholder' => 'Да',
	'is_free_yes' => 'Да',
	'is_free_no' => 'Нет',
    'f_published' => 'Опубликованный',
    'f_published_on' => 'опубликовать',
	'f_content' => 'Описание',
	'f_image' => 'Изображение',
	'h_image' => 'Стоп-кадр видео. Рекомендуемый размер 640x372 px',
    'f_file' => 'Материалы к видео',
	'h_file' => '',
	'f_code' => 'Видео код',
	'h_code' => 'В виде ссылки. Например, бесплатное видео - “https://youtu.be/oUd8simf0x0”, платное видео - “https://player.vimeo.com/video/211306014”',
	'f_text_short' => 'Короткое описание',
    'delete_operation_denied' => 'Для видео :name найдены заказ(ы). В операции удаления отказано'
];
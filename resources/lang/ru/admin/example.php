<?php

return [
    'list' => 'Список примеров',
    'add' => 'Добавить пример',
    'add_btn' => 'Добавить',
    'edit' => 'Редактировать пример',
    'sort' => 'Сортировать примеры',
    'sort_btn' => 'Сортировка',
    'saved' => 'Пример сохранен',
    'sorted' => 'Примеры отсортированы',

    'visible' => 'Видимый',
    'published' => 'Опубликованый',
    'hidden' => 'Скрытый',

    'f_title' => 'Заголовок',
    'f_permalink' => 'Ссылка',
    'f_published_at' => 'Дата',
    'f_published' => 'Опубликован',
    'f_published_on' => 'опубликовано',
    'f_image' => 'Картинка',
    'f_content' => 'Контент',
    'h_image' => 'рекомендуемый размер 100x100 px'
];
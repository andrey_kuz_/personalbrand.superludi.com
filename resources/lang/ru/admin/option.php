<?php

return [
    'course_expires' => 'Срок активности курса',
    'course_expires_description' => '',
	'course_package_price' => 'Стоимость пакета',
    'course_package_price_description' => '',
	'course_price_rate' => 'Курс валюты',
    'course_price_rate_description' => 'Цена единицы стоимости курса в гривне',
];
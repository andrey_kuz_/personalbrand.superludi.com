import '../assets/scss/style.scss';

import '../assets/js/init';
import '../assets/js/animations';
import '../assets/js/sliders';
import '../assets/js/actions';
import '../assets/js/accordion';
import '../assets/js/fixed';
import '../assets/js/forms';

import '../assets/js/csrf_token';
import '../assets/js/access';
import '../assets/js/auth';
import '../assets/js/video';
import '../assets/js/comments';
import '../assets/js/subscribe';
import '../assets/js/payment';
import '../assets/js/utm';
import '../assets/js/popup';

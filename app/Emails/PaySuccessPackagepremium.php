<?php

namespace App\Emails;

use AltSolution\Admin\EmailTemplate\Template;

class PaySuccessPackagepremium extends Template
{
    protected $view = 'emails.pay_success_package_premium';
    protected $name = 'PaySuccessPackagepremium';
    protected $description = 'Premium package paid template';

    public function init()
    {
        $this->setLegend([
            'user.email' => "User's email",
			'user.name' => "User's name",
			'package.name' => 'Package name',
			'site.default.title' => 'Default site title',
        ]);
        $this->setNameTo('');
        $this->setNameFrom(config('mail.from.name'));
    }
}
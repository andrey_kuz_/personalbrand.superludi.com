<?php

namespace App\Emails;

use AltSolution\Admin\EmailTemplate\Template;

class Newsletter extends Template
{
    protected $view = 'emails.newsletter';
    protected $name = 'Newsletter';
    protected $description = 'Newsletter user template';

    public function init()
    {
        $this->setLegend([
            'user.email' => "User's email",
			'site.default.title' => 'Default site title',
        ]);
        $this->setNameTo('');
        $this->setNameFrom(config('mail.from.name'));
    }
}
<?php

namespace App\Emails;

use AltSolution\Admin\EmailTemplate\Template;

/**
 * Example of email template
 */
class Example extends Template
{
    protected $name = 'Example';
    protected $description = 'Example of email template';
    //protected $isMultilingual = true;
    protected $view = 'emails.example';

    protected function init()
    {
        $this->setLegend([
            'message' => 'Message text',
        ]);
        $this->setNameFrom('noreply');
        $this->setNameTo('Admin');
    }
}
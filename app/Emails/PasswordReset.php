<?php

namespace App\Emails;

use AltSolution\Admin\EmailTemplate\Template;

class PasswordReset extends Template
{
    protected $view = 'emails.password-reset';
    protected $name = 'PasswordReset';
    protected $description = 'Reset user password template';

    public function init()
    {
        $this->setLegend([
            'user.email' => "User's email",
            'user.name' => "User's name",
            'user.password' => "User's password",
			'site.default.title' => 'Default site title',
        ]);
        $this->setNameTo('');
        $this->setNameFrom(config('mail.from.name'));
    }
}
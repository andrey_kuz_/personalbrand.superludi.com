<?php

/*
|--------------------------------------------------------------------------
| Application Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Routing\Router;
/** @var Router $router */
/*
 * Example
 */

$router->get('example', 'ExampleController@index')->name('example_list');
$router->get('example/edit/{id?}', 'ExampleController@edit')->name('example_edit');
$router->post('example/save', 'ExampleController@save')->name('example_save');
$router->post('example/action', 'ExampleController@action')->name('example_action');
$router->get('example/sort', 'ExampleController@sort')->name('example_sort');
$router->post('example/sort', 'ExampleController@saveSort');
/*
 * Video
 */
$router->get('video', 'VideoController@index')->name('video_list');
$router->get('video/edit/{id?}', 'VideoController@edit')->name('video_edit');
$router->post('video/save', 'VideoController@save')->name('video_save');
$router->post('video/action', 'VideoController@action')->name('video_action');
$router->get('video/sort', 'VideoController@sort')->name('video_sort');
$router->post('video/sort', 'VideoController@saveSort');
/*
 * Order
 */


$router->get('order', 'OrderController@index')->name('order_list');
$router->get('order/history/{id?}', 'OrderController@history')->name('order_history');
/*
$router->get('section','SectionController@all')->name('section_list');
$router->get('section/add','SectionController@add')->name('section_add');
$router->post('section/save','SectionController@save')->name('section_save');
$router->get('section/{id}','SectionController@index')->name('section_show');
$router->post('section/update','SectionController@update')->name('section_update');
$router->post('section/action', 'SectionController@action')->name('section_action');
$router->post('section/delete/{id}','SectionController@delete')->name('section_delete');

$router->get('post','PostController@all')->name('post_list');
$router->get('post/add','PostController@add')->name('post_add');
$router->post('post/save','PostController@save')->name('post_save');
$router->get('post/{id}','PostController@index')->name('post_show');
$router->post('post/update','PostController@update')->name('post_update');
$router->post('post/action', 'PostController@action')->name('post_action');
$router->post('post/delete/{id}','PostController@delete')->name('post_delete');


$router->get('banner','BannerController@all')->name('banner_list');
$router->get('banner/add','BannerController@add')->name('banner_add');
$router->post('banner/save','BannerController@save')->name('banner_save');
$router->get('banner/{id}','BannerController@index')->name('banner_show');
$router->post('banner/update','BannerController@update')->name('banner_update');
$router->post('banner/action', 'BannerController@action')->name('banner_action');
$router->post('banner/delete/{id}','BannerController@delete')->name('banner_delete');
*/
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Routing\Router;
/** My routes Ukrosoft*/

if (env('APP_ENV') === 'production') {
    URL::forceSchema('https');
}

$router->post('mail', ['as' => 'send_mail', 'uses' => 'MailController@send']);

/** @var Router $router */
$router->post('/video_point','VideoPointController@index');
// Access Check
$router->post('access', ['as' => 'access', 'uses' => 'Auth\AuthController@accessCheck']);
//
$router->get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
$router->get('content/{permalink}', ['as' => 'content', 'uses' => 'ContentController@view']);
$router->get('videos/{permalink}', ['as' => 'video_detail', 'uses' => 'VideoController@view']);
$router->get('videos', ['as' => 'video_list', 'uses' => 'VideoController@view']);
$router->get('videos-to-pay/{package}', ['as' => 'video_pay', 'uses' => 'VideoController@toPay']);

$router->get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
$router->get('login', function (){ return redirect('/'); });
$router->post('ajaxlogin', ['as' => 'ajaxlogin', 'uses' => 'Auth\AuthController@ajaxLogin']);

$router->get('socialite/redirect/{provider}', ['as' => 'socialite.redirect', 'uses' => 'Auth\SocialiteController@redirect']);
$router->get('socialite/callback/{provider}', ['as' => 'socialite.callback', 'uses' => 'Auth\SocialiteController@callback']);

$router->post('user/create', ['as' => 'user_create', 'uses' => 'Auth\AuthController@newUser']);
$router->post('user/registration', ['as' => 'user_registration', 'uses' => 'Auth\AuthController@registration']);
$router->post('user/password-reset', ['as' => 'password_reset', 'uses' => 'Auth\PasswordController@passwordReset']);
$router->post('order/create', ['as' => 'order_create', 'uses' => 'OrderController@newOrder']);
$router->post('order/create_pdf', ['as' => 'create_invoice', 'uses' => 'OrderController@create_pdf']);


$router->get('pay/success', ['as' => 'pay_success', 'uses' => 'PayController@success']);
$router->get('pay/fail', ['as' => 'pay_fail', 'uses' => 'PayController@fail']);
$router->post('pay/callback', ['as' => 'pay_callback', 'uses' => 'PayController@callback']);



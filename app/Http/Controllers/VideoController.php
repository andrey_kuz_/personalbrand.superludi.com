<?php

namespace App\Http\Controllers;

use App\Models\VideoPoint;
use Illuminate\Http\Request;
use App\Models\Video;
use App\Models\Package;

class VideoController extends Controller
{
	public function __construct(Request $request)
    {
        parent::__construct($request);
		
		$this->middleware('auth');
	}
	
	public function view($permalink = '')
	{
		$user = \Auth::user();
		if(empty($user) || is_object($user) && empty($user->id))
		{
			return redirect(route('index'));
		}
        $is_selected = 1;
		$video_point = 0;
		$videoPointInfo = VideoPoint::where('user_id',$user->id)->first();
        if ($videoPointInfo) {
            $is_selected = substr($videoPointInfo->video_id, 6);
            $video_point = $videoPointInfo->time;
        }

		$packages = Package::availables();
		
		$videos = Video::available()->sorting()->get();
		$episode_count = count(Video::videosAvailable());
		$is_paid = !empty($user->paidVideos());
		
		$free_video_ids = Video::available()->free()->lists('id')->toArray();
		
		$hide_free_button = true;
        $paid_videos = $user->paidVideos();
        $route_name = \Request::route()->getName();
        $cabinet = true;

		return view('video.view', compact('free_video_ids', 'videos', 'episode_count', 'is_paid', 'packages', 'is_selected', 'video_point', 'hide_free_button', 'paid_videos', 'route_name', 'cabinet'));
    }
	
	public function toPay($package_to_pay)
	{
		$user = \Auth::user();
		if(empty($user) || is_object($user) && empty($user->id))
		{
			return redirect(route('video_list'));
		}
		
		$packages = Package::availables();
		if(empty($packages[$package_to_pay]))
		{
			return redirect(route('video_list'));
		}
		
		$package_to_pay = 'package_'.$package_to_pay;
		
		return redirect(route('index'))->with(compact('package_to_pay'));
	}
}

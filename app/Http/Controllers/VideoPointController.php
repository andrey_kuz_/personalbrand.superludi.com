<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\VideoPoint;

class VideoPointController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request,[
            'video_id' => 'required|string',
            'time' => 'required|integer',
            ]
        );
        $user_id = \Auth::user()->id;
        if ($user_id) {
            VideoPoint::where('user_id', $user_id)->delete();
            $videoPoint = new VideoPoint;
            $videoPoint->user_id = $user_id;
            $videoPoint->video_id = $request->video_id;
            $videoPoint->time = $request->time;
            $videoPoint->save();
        }
    }
}

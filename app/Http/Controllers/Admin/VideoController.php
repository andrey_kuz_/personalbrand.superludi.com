<?php

namespace App\Http\Controllers\Admin;

use App\Forms\VideoForm;
use App\Models\Video;
use App\Models\Basket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use AltSolution\Admin\Http\Controllers\Controller;

class VideoController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('permission', 'video.list');

        $q = Video::query();

        $filter = [
            'q' => $request->input('q'),
            'visible' => $request->input('visible'),
            'sort' => $request->input('sort', Video::$sortBy.'-'.Video::$sortDir),
        ];
		
        if ($filter['q']) {
            $q->search($filter['q']);
        }
        if ($filter['visible']) {
            $q->where('is_published', $filter['visible'] == 1);
        }
        if (empty($filter['sort'])) {
            $filter['sort'] = 'id-desc';
        }
        list($sortBy, $sortDir) = explode('-', $filter['sort']);
        if ($sortBy == 'abc') {
            $q->orderBy('title_' . config('app.locale'), $sortDir);
        } else {
            $q->orderBy($sortBy, $sortDir);
        }

        $items = $q->paginate(config('admin.item_per_page', 20));
        $items->addQuery('q', $filter['q']);
		$items->appends($request->input());
		
        $this->layout
            ->setActiveSection('video')
            ->setTitle(trans('admin/video.list'));

        return view('admin/video.list', compact('items', 'filter'));
    }

    public function edit($id = null)
    {
        $this->authorize('permission', 'video.edit');

        $video = Video::query()->findOrNew($id);
        if (!$id) {
            $video['is_published'] = true;
        }

        $form = app(VideoForm::class)->create($video);

        $this->layout
            ->setActiveSection('video')
            ->setTitle(trans($video ? 'admin/video.edit' : 'admin/video.add'))
            ->addBreadcrumb(trans('admin/video.list'), route('admin/video_list'));
        return view('admin/video.edit', compact('id', 'video', 'form'));
    }

    public function save(Request $request)
    {
        $this->authorize('permission', 'video.edit');

		$item = Video::query()->firstOrNew(['id' => $request->input('id')]);
		$is_published_old = $item->is_published;
        $locale = config('app.locale');
        $this->validate(
			$request,
			[
				'title_'.$locale => 'required',
				'text_short_'.$locale => 'required',
				'permalink' => 'required|unique:videos,permalink,'.$item->id,
				'code' => 'required|wistia',
				'image' => (!empty($item->id) ? '' : 'required|').'image|max:10000|image_size:>=100,>=100|image_size:<5000,<5000',
			],
			[],
			[
				'image' => trans('admin/video.f_image'),
				'file' => trans('admin/video.f_file'),
				'code' => trans('admin/video.f_code'),
				'permalink' => trans('admin/video.f_permalink'),
			]	
		);

        $item->fill($request->all());
        $item->imageAllSave($request);
		$item->uploadAllSave($request);
        if (!$item->exists) {
            $item['sort'] = Video::query()->max('sort') + 1;
        }
		if(empty($is_published_old) && !empty($item->is_published))
		{
			$item->published_at = new Carbon;
		}	
        $item->save();

        $this->layout->addNotify('success', trans('admin/video.saved'));

        if ($request['button_apply']) {
            return redirect()->route('admin/video_edit', ['id' => $item['id']]);
        }

        return redirect()->route('admin/video_list');
    }

    public function delete($id = null)
    {
		$this->authorize('permission', 'video.delete');
		
		if(empty($id))
		{
			return redirect()->route('admin/video_list');
		}	
		
        $video = Video::query()->find($id);
		if(empty($video))
		{
			return redirect()->route('admin/video_list');
		}	
		
		if(Basket::where('video_id', $video->id)->exists())
		{	
			$this->layout->addNotify('warning', trans('admin/video.delete_operation_denied', ['name' => $video->tarns('title')]));
			return redirect()->route('admin/video_list');
		}	
        
        $video->delete();

        return redirect()->route('admin/video_list');
    }

    public function action(Request $request)
    {
        $this->authorize('permission', 'video.edit');

        $action = $request->input('action');
        $itemIds = $request->input('ids');
        if (!$itemIds) {
            return;
        }
        foreach ($itemIds as $itemId) {
            $item = Video::query()->findOrFail($itemId);
            switch ($action) {
                case 'publish':
                    $item['is_published'] = true;
					$item->published_at = new Carbon;
                    $item->save();
                    break;
                case 'hide':
                    $item['is_published'] = false;
                    $item->save();
                    break;
				case 'delete': 
				{	
					if(Basket::where('video_id', $itemId)->exists())
					{	
						$this->layout->addNotify('warning', trans('admin/video.delete_operation_denied', ['name' => 'ID='.$itemId]));
						break;
					}

                    $item->imageAllDelete();
					$item->uploadAllDelete();
                    $item->delete();
				}	
                break;
            }
        }
    }

    public function sort()
    {
        $this->authorize('permission', 'video.edit');

        $items = Video::query()
            ->orderBy('sort')
            ->get();

        $this->layout
            ->setActiveSection('video')
            ->setTitle(trans('admin/video.sort'))
            ->addBreadcrumb(trans('admin/video.list'), route('admin/video_list'));

        return view('admin/video.sort', compact('items'));
    }

    public function saveSort(Request $request)
    {
        $this->authorize('permission', 'video.edit');

        $itemIds = $request->input('ids');
        if (!$itemIds) {
            return;
        }
        foreach ($itemIds as $index => $itemId) {
            $item = Video::query()->findOrFail($itemId);
            $item['sort'] = $index;
            $item->save();
        }

        return response()->json([
            'success' => true,
            'message' => trans('admin/video.sorted'),
        ]);
    }

}

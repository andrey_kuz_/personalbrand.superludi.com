<?php

namespace App\Http\Controllers\Admin;

use App\Forms\ExampleForm;
use App\Models\Example;
use Carbon\Carbon;
use Illuminate\Http\Request;
use AltSolution\Admin\Http\Controllers\Controller;

class ExampleController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('permission', 'example.list');

        $q = Example::query();

        $filter = [
            'q' => $request->input('q'),
            'visible' => $request->input('visible'),
            'sort' => $request->input('sort'),
        ];

        if ($filter['q']) {
            $q->search($filter['q']);
        }
        if ($filter['visible']) {
            $q->where('is_published', $filter['visible'] == 1);
        }
        if (empty($filter['sort'])) {
            $filter['sort'] = 'id-desc';
        }
        list($sortBy, $sortDir) = explode('-', $filter['sort']);
        if ($sortBy == 'abc') {
            $q->orderBy('title_' . config('app.locale'), $sortDir);
        } else {
            $q->orderBy('id', $sortDir);
        }

        $items = $q->paginate(config('admin.item_per_page', 20));
        $items->addQuery('q', $filter['q']);

        $this->layout
            ->setActiveSection('example')
            ->setTitle(trans('admin/example.list'));

        return view('admin/example.list', compact('items', 'filter'));
    }

    public function edit($id = null)
    {
        $this->authorize('permission', 'example.edit');

        $example = Example::query()->findOrNew($id);
        if (!$id) {
            $example['is_published'] = true;
            $example['published_at'] = Carbon::now()->format('m/d/Y');
        }

        $form = app(ExampleForm::class)->create($example);

        $this->layout
            ->setActiveSection('example')
            ->setTitle(trans($example ? 'admin/example.edit' : 'admin/example.add'))
            ->addBreadcrumb(trans('admin/example.list'), route('admin/example_list'));
        return view('admin/example.edit', compact('id', 'example', 'form'));
    }

    public function save(Request $request)
    {
        $this->authorize('permission', 'example.edit');

        $itemId = $request['id'];
        $locale = config('app.locale');
        $this->validate($request, [
            'title_' . $locale => 'required',
            'permalink' => 'required',
            'image' => ($itemId ? '' : 'required|' ) . 'image|max:2000|image_size:>=100,>=100|image_size:<3000,<3000',
            'content_' . $locale => 'required',
        ]);

        $item = Example::query()->firstOrNew(['id' => $itemId]);
        $item->fill($request->all());
        //$example->user_id = \Auth::user()->id;
        $item->imageAllSave($request);
        if (!$item->exists) {
            $item['sort'] = Example::query()->max('sort') + 1;
        }
        $item->save();

        $this->layout->addNotify('success', trans('admin/example.saved'));

        if ($request['button_apply']) {
            return redirect()->route('admin/example_edit', ['id' => $item['id']]);
        }

        return redirect()->route('admin/example_list');
    }

    public function delete($id = null)
    {
        $this->authorize('permission', 'example.delete');

        $example = Example::query()->find($id);
        if ($example) {
            $example->delete();
        }

        return redirect()->route('admin/example_list');
    }

    public function action(Request $request)
    {
        $this->authorize('permission', 'example.edit');

        $action = $request->input('action');
        $itemIds = $request->input('ids');
        if (!$itemIds) {
            return;
        }
        foreach ($itemIds as $itemId) {
            $item = Example::query()->findOrFail($itemId);
            switch ($action) {
                case 'publish':
                    $item['is_published'] = true;
                    $item->save();
                    break;
                case 'hide':
                    $item['is_published'] = false;
                    $item->save();
                    break;
                case 'delete':
                    $item->imageAllDelete();
                    $item->delete();
                    break;
            }
        }
    }

    public function sort()
    {
        $this->authorize('permission', 'example.edit');

        $items = Example::query()
            ->orderBy('sort')
            ->get();

        $this->layout
            ->setActiveSection('example')
            ->setTitle(trans('admin/example.sort'))
            ->addBreadcrumb(trans('admin/example.list'), route('admin/example_list'));

        return view('admin/example.sort', compact('items'));
    }

    public function saveSort(Request $request)
    {
        $this->authorize('permission', 'example.edit');

        $itemIds = $request->input('ids');
        if (!$itemIds) {
            return;
        }
        foreach ($itemIds as $index => $itemId) {
            $item = Example::query()->findOrFail($itemId);
            $item['sort'] = $index;
            $item->save();
        }

        return response()->json([
            'success' => true,
            'message' => trans('admin/example.sorted'),
        ]);
    }

}

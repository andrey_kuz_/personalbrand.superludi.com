<?php
namespace App\Http\Controllers\Admin;

use AltSolution\Admin\Http\Controllers\ElfinderController as BaseElfinderController;

class ElfinderController extends BaseElfinderController {

	// Documentation for connector options:
	// https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options
	public function opts() {
		$upload_folder = 'uploads/elfinder';
		return array(
		// 'debug' => true,
		'roots' => array(
			array(
				'driver'        => 'LocalFileSystem',           // driver for accessing file system (REQUIRED)
				'path'          => public_path($upload_folder),                 // path to files (REQUIRED)
				'URL'           => url($upload_folder),//'/uploads', // URL to files (REQUIRED)
				'uploadDeny'    => array('all'),                // All Mimetypes not allowed to upload
				'uploadAllow'   => array('image', 'text/plain', 'application/pdf'),// Mimetype `image` and `text/plain` allowed to upload
				'uploadOrder'   => array('deny', 'allow'),      // allowed Mimetype `image` and `text/plain` only
				'accessControl' => [$this, 'access']                     // disable and hide dot starting files (OPTIONAL)'
				)
			)
		);
	}

	// run elFinder
	//$connector = new elFinderConnector(new elFinder($opts));
	//$connector->run();
}
<?php

namespace App\Http\Controllers\Admin;

use AltSolution\Admin\Http\Controllers\UsersController as BaseController;
use AltSolution\Admin\Models\AclRole;
use App\Forms\UserForm;
use App\Models\Basket;
use App\Models\Order;
use App\Models\User;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use AltSolution\Admin\Seo\SeoManagerInterface;
use AltSolution\Admin\Models\Option;


class UsersController extends BaseController
{
    public function getEdit($id = null)
    {
        $this->authorize('permission', 'user.edit');
    	$user = null;

    	if ($id) {
    		$user = User::query()->find($id);
    	}

    	$acl_roles = AclRole::all();

        $form = app(UserForm::class)->create($user);

        $data = [
            'edit_user' => $user,
            'acl_roles' => $acl_roles,
            'form' => $form,
        ];

        $this->layout
            ->setActiveSection('users')
            ->setTitle(trans($user ? 'admin::user.edit' : 'admin::user.add'))
            ->addBreadcrumb(trans('admin::user.title'), route('admin::user_list'));
    	return view('admin::users.edit', $data);
    }

    public function postReset(Request $request) {
        $rules = [
            'id' => 'required|exists:users'
            ];
        $this->validate($request, $rules);
        $user = User::find($request->id);
        $user_admin = User::find(\Auth::user()->id);
        $password = str_random(12);
        $password_bcrypt = bcrypt($password);
        if(($user->id != $user_admin->id) && ($user_admin->acl_role_id == 3) && $password ) {
            $user->password =  $password_bcrypt;
            if($user->save()) {
                $is_send_reset = cms_send_template('PasswordReset', [
                    'user.email' => $user->email,
                    'user.name' => $user->name,
                    'user.password' => $password,
                    'site.default.title' => app(SeoManagerInterface::class)->getDefaultTitle(),
                ]);
                $is_send_reset_admin = cms_send_template('PasswordReset', [
                    'user.email' => $user_admin->email,
                    'user.name' => $user->name,
                    'user.password' => $password,
                    'site.default.title' => app(SeoManagerInterface::class)->getDefaultTitle(),
                ]);
            }

            return response()->json(['message' => 'Пароль пользователя обновлён'], 200);
        } else {
            return response()->json(['message' => 'У вас нет прав для обновления'], 400);
        }

    }



    public function postSave(Request $request)
    {
        $this->authorize('permission', 'user.edit');
        $user = User::query()->firstOrNew(['id' => $request->id]);
        $rules = [
            'name' => 'required|max:100',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone' => 'required',
            'package_name' => 'required',
            'avatar_file' => 'image|max:1000|image_size:>=128,>=128|image_size:<6000,<6000',
            'password' => array_merge($request->id ? [] : ['required'], [
                'confirmed',
                'min:6',
                'max:60',
                'regex:/[a-z]/', // letter lower
                'regex:/[A-Z]/', // letter upper
                'regex:/[0-9]/', // number
                'regex:/[!@#$%^&*?()\-_=+{};:,<.>]/' // special
            ]),
            'password_confirmation' => $request->id ? '' : 'required',
        ];
        if($request->paid) {
            unset($rules['password']);
            unset($rules['password_confirmation']);
        } else {
            unset($rules['package_name']);
        }

    	$this->validate($request, $rules, [
    	    'password.regex' => trans('admin::user.password_sux'),
        ]);

        /** @var User $user */
        $user = User::query()->firstOrNew(['id' => $request->id]);
        $user->fill($request->except('password'));

        if ($request->avatar_file_delete) {
            $user->imageDelete('avatar_file');
        }
        if ($request->file('avatar_file')) {
            $user->imageDelete('avatar_file');
            $user->imageSave('avatar_file', $request->file('avatar_file'));
        }
        if ($request->password) {
          $user->password = bcrypt($request->password);
        } else {
            $password = str_random(12);
            $user->password = bcrypt($password);
        }
        $user->id == \Auth::user()->id ? $user->active = 1 : null;
        $user_admin = User::find(\Auth::user()->id);

        //send password
        if(($user->id != $user_admin->id) && ($user_admin->acl_role_id == 3) && $request->password ) {
            $is_send_reset = cms_send_template('PasswordReset', [
                'user.email' => $user->email,
                'user.name' => $user->name,
                'user.password' => $request->password,
                'site.default.title' => app(SeoManagerInterface::class)->getDefaultTitle(),
            ]);
        }

    	$user->save();
    	if ($request->paid) {
            $order = Order::where('user_id', $user->id)->where('payed', true)->first();
    	    if(!$order) {
                $sale = 0;
                $get_rate = Option::query();
                $option = Option::query();
                $order = new Order;
                $price_rate = $get_rate->where('name', 'course_price_rate')->pluck('value')->first();
                $order->user_id = $user->id;
                $order->order_number = 'create paid form admin';
                $order->package_name = $request->package_name;
                switch($request->package_name) {
                    case 'GOOD':
                        $sale = $option->where('name', 'course_package_cheap_price')->pluck('value')->first();
                        break;
                    case 'BEST':
                        $sale = $option->where('name', 'course_package_good_price')->pluck('value')->first();
                        break;
                    case 'PREMIUM':
                        $sale = $option->where('name', 'course_package_premium_price')->pluck('value')->first();
                        break;
                }
                $order->price = (int)($sale * $price_rate);
                $order->payed = true;
                $order->save();
                $video_id = Video::select('id')->where('is_free', 0)->first();
                $basket = new Basket;
                $basket->order_id = $order->id;
                $basket->video_id = $video_id->id;
                $basket->save();
                $update_user = User::find($user->id);
                $password = str_random(12);
                $update_user->password = Hash::make($password);
                $update_user->save();
                if(!(empty($update_user->email) && empty($password)))
                {
                    $is_send = cms_send_template('MadePaid', [
                        'user.email' => $update_user->email,
                        'user.name' => $update_user->name,
                        'user.password' => $password,
                        'site.default.title' => app(SeoManagerInterface::class)->getDefaultTitle(),
                    ]);
                }
            }
        } else {
            $orders = Order::where('user_id', $user->id)->where('payed', true)->where('order_number','create paid form admin')->get();
            if(count($orders)) {
                $order_ids = array();
                foreach ($orders as $order) {
                    $order_ids[] = $order->id;
                }
                Order::whereIn('id', $order_ids)->delete();
                Basket::whereIn('order_id', $order_ids)->delete();
            }
        }

    	$this->layout->addNotify('success', trans('admin::user.saved'));

        if ($request['button_apply']) {
            return redirect()->route('admin::user_edit', ['id' => $user['id']]);
        }

        return redirect()->route('admin::user_list');
    }
	
	public function getDelete($id = null)
	{
       	$this->authorize('permission', 'user.delete');
		
		if(intval($id) <= 0)
		{
			return redirect('admin/users');
		}	
		
		$user = User::find($id);
		if(empty($user))
		{
			return redirect('admin/users');
		}	
		
		$orders = $user->orders()->lists('id')->toArray();
		if(!empty($orders))
		{
			$this->layout->addNotify('warning', trans('admin/user.delete_operation_denied', ['name' => $user->name]));
			return redirect('admin/users');
		}	
		
		$user->imageAllDelete();
		$user->delete();
				
        return redirect('admin/users');
    }
	
	public function postMassdelete(Request $request) 
	{
        $this->authorize('permission', 'user.delete');
        if(empty($request->data))
		{
            return;
        }
		
		foreach ($request->data as $user_id)
		{
			if(intval($user_id) <= 0 || $user_id == \Auth::user()->id)
			{
				continue;
			}
			
			$user = User::find($user_id);
			if(empty($user))
			{	
				continue;
			}	
			
			$orders = $user->orders()->lists('id')->toArray();
			if(!empty($orders))
			{
				$this->layout->addNotify('warning', trans('admin/user.delete_operation_denied', ['name' => $user->name]));
				continue;
			}
			
			$user->imageAllDelete();
			$user->delete();
				
        }
		
    }
}	

<?php
/**
 * Created by PhpStorm.
 * User: aku
 * Date: 14.07.18
 * Time: 11:43
 */

namespace App\Http\Controllers;

use App\Services\UniSenderService;
use Illuminate\Http\Request;
use App\Services\MailChimpService;



class MailController extends Controller
{
    public function send (Request $request){



        $this->validate($request,
            [
                'email' => 'required|email'
            ],
            array(
                'email.required'=> trans('mail.required'),
                'email.email'=> trans('mail.email'),
            )
        );
        
        $mailschimp = new MailChimpService();
        $mailschimp_add = $mailschimp->send($request->email);

        $unisender = new UniSenderService();
        $response = $unisender->send($request->email);

        echo $response;


    }

}
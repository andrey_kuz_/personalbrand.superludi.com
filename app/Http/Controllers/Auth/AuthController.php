<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Http\Controllers\Controller;
use AltSolution\Admin\Models\AclRole;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use AltSolution\Admin\Seo\SeoManagerInterface;
use App\Models\User;
use App\Models\TempUserPack;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => ['logout','accessCheck']]);
    }

    public function registration(Request $request)
    {
        if(!$request->ajax())
        {
            return redirect(route('index'));
        }

        $result = [
            'success' => 1,
            'message' => 'Unknown message',
            'user_info' => [],
        ];

        $email = $request->input('email');
        $password = $request->input('password');
        $promocode = $request->input('promocode');
        $data = [
            'name' => $email,
            'email' => $email,
            'password' => $password,
            'promocode' => $promocode
        ];

        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email,'.$email.'|email|max:255',
            'password' => 'required|min:6',
        ]);
		if($validator->fails())
		{
			$result['success'] = 0;
			$errors = $this->MessagesToString($validator->messages()->toArray());
			if(!empty($errors))
			{	
				$result['message'] = $errors;
			}	
		}
		
		if(!empty($result['success']))
		{
			$result['user_info'] = $data;
			$result['user_info']['redirect'] = route('video_list');
		}
		
		return Response::json($result);
	}	
	
	public function newUser(Request $request)
	{
		if(!$request->ajax())
		{
			return redirect(route('index'));
		}

		$result = [
			'success' => 1,
			'message' => 'Unknown message',
			'user_id' => 0,
		];

		$new_user = NULL;
		$is_auto_login = $request->input('is_auto_login');
		$user_password = $request->input('password', str_random(6));
		$user_data = $request->all() + ['password' => $user_password];
		$validator = $this->validator($user_data);
		if($validator->fails())
		{
			$result['success'] = 0;
			$errors = $this->MessagesToString($validator->messages()->toArray());
			if(!empty($errors))
			{
				$result['message'] = $errors;
			}
		}

		if(!empty($result['success']))
		{
			$user_exist = User::where('email', $user_data['email'])->first();
			if(!empty($user_exist))
			{
				$result['success'] = 0;
				$result['message'] = trans('auth.email_unique');
			}
		}

		if(!empty($result['success']) && empty($new_user))
		{
			$new_user = $this->create($user_data + ['active' => 1] + ['site' => 'personalbrand']);

			//Save name pack. from cookie
			$user_pack_cookie = TempUserPack::query()->firstOrNew(['id_user' => $new_user['id']]);
			if(isset($_COOKIE['pack'])) {
				$user_pack_cookie->name_pack = $_COOKIE['pack'];
				$user_pack_cookie->save();
			}
		}

		if(!empty($result['success']) && empty($new_user->id))
		{
			$result['success'] = 0;
			$result['message'] = trans('auth.error_create_user');
		}

		if(!empty($result['success']) && empty($user_exist))
		{
			$is_send = cms_send_template('RegisterComplete', [
				'user.email' => $new_user->email,
				'user.name' => $new_user->name,
				'user.password' => $user_password,
				'site.default.title' => app(SeoManagerInterface::class)->getDefaultTitle(),
			]);

			if(empty($is_send))
			{
				$result['success'] = 0;
				$result['message'] = trans('auth.error_sending_message');
			}
		}

		if(!empty($result['success']))
		{
			$result['message'] = empty($user_exist) ? trans('auth.user_successfully_created', ['email' => $new_user->email]) : trans('auth.user_found');
			$result['user_id'] = $new_user->id;
			if($is_auto_login)
			{
				auth()->login($new_user);
			}
		}
        return Response::json($result);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'password' => 'required|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $role = AclRole::where('name', 'login')->first();
        if(empty($role))
        {
            return FALSE;
        }

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
			'acl_role_id' => $role->id,
			'active' => $data['active'],
            'site' => $data['site'],
           /* 'promocode' => $data['promocode'], */
        ]);
    }

    private function MessagesToString($data)
    {
        $result = array();
        if(!is_array($data))
        {
            return $result;
        }

        foreach($data as $name_field=>$messages)
        {
            if(!is_array($messages))
            {
                continue;
            }

            $result = array_merge($result, $messages);
        }

        return is_array($result) && !empty($result) ? implode('<br>', $result) : array();

    }

    protected function getCredentials(Request $request)
    {
        $request['active'] = 1;
        return $request->only($this->loginUsername(), 'password', 'active');
    }

    public function ajaxLogin(Request $request)
    {
        if(!$request->ajax())
        {
            return redirect(route('index'));
        }

        $result = [
            'success' => 0,
            'message' => trans('auth.please_check_entered_data'),
            'redirect' => '',
        ];

        try {
            $this->validateLogin($request);
        } catch(\Exception $e) {
            $result['message'] = $e->getMessage();
            return Response::json($result);
        }
        if ($this->checkActive($request)) {
            $result['message'] = trans('auth.user_access_denied');
            return Response::json($result);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $seconds = $this->secondsRemainingOnLockout($request);
            $result['message'] = trans('auth.too_many_login_attempts', ['seconds' => $seconds]);
            return Response::json($result);
        }

        if ($request->input('password')) {
            User::updatePassword($request->input('email'), $request->input('password'));
        }

        $credentials = $this->getCredentials($request);
        if(Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            $this->handleUserWasAuthenticated($request, $throttles);
            $result['success'] = 1;
            $result['message'] = trans('auth.login_successful');
            $result['redirect'] = route('video_list');
            return Response::json($result);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return Response::json($result);
    }

    public function checkActive(Request $request) {
        $active = 0 ;
        if ($request->email) {
            $user = User::where('email',$request->email)->first();
            if ($user) {
                $active = $user->active;
            } else {
                $active = true;
            }
        }
        if ($active) {
            return false;
        }
        return true;
    }

    public function accessCheck() {
        $id = Auth::id();
        if ($id) {

            $user = User::find($id);
            if (!$user->active) {
                Auth::logout();
                return 'banned';
            }
        }
        return 'Not banned';
    }

}
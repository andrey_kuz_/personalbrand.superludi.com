<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use AltSolution\Admin\Seo\SeoManagerInterface;
use App\Models\User;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
	
	public function passwordReset(Request $request)
	{
		if(!$request->ajax())
		{
			return redirect(route('index'));
		}
		
		$result = [
			'success' => 1,
			'message' => 'Unknown message',
		];
		
		$validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);
		if($validator->fails())
		{
			$result['success'] = 0;
			$errors = $this->MessagesToString($validator->messages()->toArray());
			if(!empty($errors))
			{	
				$result['message'] = $errors;
			}	
		}
		
		$user = NULL;
		if(!empty($result['success']))
		{	
			$user = User::where('email', $request->input('email'))->first();
			if(empty($user))
			{
				$result['success'] = 0;
				$result['message'] = trans('password.email_not_found');
			}	
		}	
		
		$password_reset = '';
		if(!empty($result['success']))
		{
			$password_reset = str_random(6);
			$user->password_reset = bcrypt($password_reset);
			if(!$user->save())
			{
				$result['success'] = 0;
				$result['message'] = trans('password.error_generating_password');
			}	
		}
		
		if(!empty($result['success']))
		{
			$is_send = cms_send_template('PasswordReset', [
				'user.email' => $user->email,
				'user.name' => $user->name,
				'user.password' => $password_reset,
				'site.default.title' => app(SeoManagerInterface::class)->getDefaultTitle(),
			]);
			
			if(empty($is_send))
			{
				$result['success'] = 0;
				$result['message'] = trans('password.error_sending_message');
			}
		}
		
		return Response::json($result);
	}
	
	private function MessagesToString($data)
	{
		$result = array();
		if(!is_array($data))
		{
			return $result;
		}	
		
		foreach($data as $name_field=>$messages)
		{
			if(!is_array($messages))
			{
				continue;
			}
			
			$result = array_merge($result, $messages);
		}
		
		return is_array($result) && !empty($result) ? implode('<br>', $result) : array();
			
	}
}

<?php

namespace App\Http\Controllers;

use AltSolution\Admin\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Log;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Basket;
use App\Models\Video;
use App\Models\User;
use App\Models\Pay;
use App\Models\Package;
use App\Models\Site;
use PDF;

class OrderController extends Controller
{
    public function newOrder(Request $request)
	{
        $user = User::find($request->input('user_id'));
        if(empty(\Auth::user()))
		{
			auth()->login($user);
		}

		if(!$request->ajax())
		{
			return redirect(route('index'));
		}

		$result = [
			'success' => 1,
			'message' => 'Unknown message',
			'form' => [],
		];

		$is_request_all_course = true;
		$packages = Package::availables();
		$request_video_id = $request->input('video_id');
		if(empty($packages) || empty($packages[$request_video_id]))
		{
			$result['success'] = 0;
			Log::error('newOrder: Error getting package "'.$request_video_id.'"');
		}

		$package_name = !empty($packages[$request_video_id]['name']) ? $packages[$request_video_id]['name'] : 'Unknown';

		$user = User::active()->find(intval($request->input('user_id', 0)));
		if(empty($user) && !empty(\Auth::user()))
		{
			$user = \Auth::user();
		}

		if(empty($user) || !is_object($user) || !($user instanceof User) || empty($user->id))
		{
			$result['success'] = 0;
			Log::error('newOrder: Error getting user');
		}

		$unpaid_videos = $user->unpaidVideos();
		if(!empty($result['success']) && !$is_request_all_course && !in_array($request_video_id, $unpaid_videos))
		{
			$result['success'] = 0;
			Log::error('newOrder: Error getting unpaid video for user', ['user_id' => $user->id]);
		}

		try
		{
			if(!empty($result['success']))
			{
				$order_video = $is_request_all_course ? $unpaid_videos : [$request_video_id];
				$order = Order::unpaidOrderByUnpaidVideos($user, $order_video);

				if(!empty($order))
				{
					$order->package_name = $package_name;
					$order->history .= "\n".'['.strval(new Carbon).'] '.'Очередная попытка оплаты заказа';
					$order->history .= "\n".'Название пакета: '.$package_name;
					$order->history .= "\n".'Пользователь: имя - '.$user->name.', id - '.$user->id.', email - '.$user->email;
					$order->history .= "\n";
					$order->save();
				}
				else {
					$order = Order::create([
						'user_id' => $user->id,
						'payed' => false,
					]);

					foreach($order_video as $order_video_id)
					{
						Basket::create([
							'order_id' => $order->id,
							'video_id' => $order_video_id,
						]);
					}

					$order = Order::unpayed()->where('id', $order->id)->where('user_id', $user->id)->first();
					$order->package_name = $package_name;
					$order->history .= "\n".'['.strval(new Carbon).'] '.'Создание заказа';
					$order->history .= "\n".'Название пакета: '.$package_name;
					$order->history .= "\n".'Пользователь: имя - '.$user->name.', id - '.$user->id.', email - '.$user->email;
					$order->history .= "\n";
					$order->save();
				}

			}
		} catch(\Exception $e) {
			$result['success'] = 0;
			Log::error('newOrder: '.$e->getMessage(), ['user_id' => $user->id]);
		}

		if(!empty($result['success']) && empty($order))
		{
			$result['success'] = 0;
			Log::error('newOrder: Error getting orders for pay', ['user_id' => $user->id]);
		}


		if(!empty($result['success']))
		{
			$info_pay = Order::infoPay($user->id, $order->id, $request_video_id);
			$result['form'] = Pay::form($info_pay);
			if(empty($result['form']))
			{
				$result['success'] = 0;
				Log::error('newOrder: Error creating pay form', ['user_id' => $user->id, 'order_id_' => $order->id]);
			}
		}

		if(!empty($result['success']))
		{
			$order->order_number = $info_pay['number'];
			$order->save();
		}

		if(empty($result['success']))
		{
			$result['message'] = trans('order.error_create_order');
		}

		return Response::json($result);
	}
	public function create_pdf(Request $request) {
        $data = $request->all();

        $price = Option::select('value')->where('name','course_package_'.$request->package.'_price')->first();
        if( $request->package == 'cheap') {
            $data['package'] = 'Пакет "GOOD"';
        }
        if ($request->package == 'good') {
            $data['package'] = 'Пакет "BEST"';
        }
        if ($request->package == 'premium') {
            $data['package'] = 'Пакет "PREMIUM"';
        }
        $data['package_price'] = $price->value.'.00 USD';
        $pdf = PDF::loadView('pdf.document', $data);
        return $pdf->stream('document.pdf');
    }

}

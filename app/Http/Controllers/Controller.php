<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Request;

use App\Services\UTMSavedService;

use AltSolution\Admin\Seo\SeoManagerInterface;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request) {
        view()->share('authUser', \Auth::user());
		$site_name = app(SeoManagerInterface::class)->getSiteName();
		view()->share('siteName', $site_name ? $site_name : trans('site.name'));
		view()->share('siteDefaultTitle', app(SeoManagerInterface::class)->getDefaultTitle());

        app(UTMSavedService::class);
    }
}

<?php

namespace App\Forms;

use AltSolution\Admin\Form;
use AltSolution\Admin\Form\Field;
use AltSolution\Admin\Form\Component;

class ExampleForm extends Form\AbstractFactory
{
    public function buildForm(Form\BuilderInterface $builder)
    {
        $builder->add('form_open', Component\FormOpen::class, [
            'method' => 'post',
            'action' => route('admin/example_save'),
            'enctype' => 'multipart/form-data',
        ]);
        $builder->add('form_submit', Component\FormSubmit::class);
        $builder->add('form_close', Component\FormClose::class);

        $builder->add('id', Field\Hidden::class);
        $builder->add('is_published', Field\Checkbox::class, [
            'label' => trans('admin/example.f_published'),
            'placeholder' => trans('admin/example.f_published_on'),
        ]);
        $builder->add('permalink', Field\Text::class, [
            'label' => trans('admin/example.f_permalink'),
            'required' => true,
        ]);
        $builder->add('published_at', Field\Date::class, [
            'label' => trans('admin/example.f_published_at'),
        ]);
        $builder->add('image', Field\Image::class, [
            'label' => trans('admin/example.f_image'),
            'help' => trans('admin/example.h_image'),
            'required' => 'unless:id',
        ]);

        $currentLocale = config('app.locale');

        foreach (cms_locales() as $locale) {
            $builder->add('title_' . $locale, Field\Text::class, [
                'label' => trans('admin/example.f_title'),
                'required' => $locale == $currentLocale,
            ]);
            $builder->add('content_' . $locale, Field\Wysiwyg::class, [
                'label' => trans('admin/example.f_content'),
                'required' => $locale == $currentLocale,
            ]);
        }
    }
}
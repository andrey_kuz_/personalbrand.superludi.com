<?php

namespace App\Forms;

use AltSolution\Admin\Form;
use AltSolution\Admin\Form\Field;
use AltSolution\Admin\Form\Component;

class VideoForm extends Form\AbstractFactory
{
    public function buildForm(Form\BuilderInterface $builder)
    {
        $builder->add('form_open', Component\FormOpen::class, [
            'method' => 'post',
            'action' => route('admin/video_save'),
            'enctype' => 'multipart/form-data',
        ]);
        $builder->add('form_submit', Component\FormSubmit::class);
        $builder->add('form_close', Component\FormClose::class);

        $builder->add('id', Field\Hidden::class);
        $builder->add('is_free', Field\Checkbox::class, [
            'label' => trans('admin/video.is_free_label'),
            'placeholder' => trans('admin/video.is_free_placeholder'),
        ]);
		$builder->add('is_published', Field\Checkbox::class, [
            'label' => trans('admin/video.f_published'),
            'placeholder' => trans('admin/video.f_published_on'),
        ]);
        $builder->add('permalink', Field\Text::class, [
            'label' => trans('admin/video.f_permalink'),
            'required' => TRUE,
        ]);
		$builder->add('image', Field\Image::class, [
            'label' => trans('admin/video.f_image'),
            'help' => trans('admin/video.h_image'),
            'required' => 'unless:id',
        ]);
		$builder->add('file', Field\Upload::class, [
            'label' => trans('admin/video.f_file'),
            'help' => trans('admin/video.h_file'),
        ]);
        $builder->add('code', Field\Textarea::class, [
            'label' => trans('admin/video.f_code'),
            'help' => trans('admin/video.h_code'),
            'required' => TRUE,
        ]);
        $currentLocale = config('app.locale');
        foreach(cms_locales() as $locale) {
            $builder->add('title_' . $locale, Field\Text::class, [
                'label' => trans('admin/video.f_title'),
                'required' => $locale == $currentLocale,
            ]);
			$builder->add('text_short_' . $locale, Field\Textarea::class, [
                'label' => trans('admin/video.f_text_short'),
                'required' => $locale == $currentLocale,
            ]);
            $builder->add('content_' . $locale, Field\Wysiwyg::class, [
                'label' => trans('admin/video.f_content'),
            ]);
        }
    }
}
<?php

namespace App\Providers;

use App\Emails;
use App\Modules\AppModule;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		//\URL::forceSchema('https');
		Validator::extend('wistia', function ($attribute, $value, $parameters, $validator) {
			return preg_match('/https:\/\/fast.wistia.net\/embed\/iframe\/[-_a-z0-9]{1,}/m', $value) === 1 /*|| preg_match('~^https://youtu.be/[-_a-z0-9]+$~i', $value) === 1*/;
		});
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->tag(AppModule::class, 'admin.system.module');
		$this->app->tag([
			Emails\RegisterComplete::class,
            Emails\MadePaid::class,
			Emails\PasswordReset::class,
			//Emails\Newsletter::class,
			/*turn off
			Emails\PaySuccessPackagecheap::class,
			Emails\PaySuccessPackagegood::class,
			Emails\PaySuccessPackagepremium::class,
			*/
		], 'admin.email-template');
       
		$this->app->bind('AltSolution\Admin\Http\Controllers\OptionsController', 'App\Http\Controllers\Admin\OptionsController');
		$this->app->bind('AltSolution\Admin\Http\Controllers\ElfinderController', 'App\Http\Controllers\Admin\ElfinderController');
		$this->app->bind('admin.module.user', \App\Modules\UserModule::class);
		
		$this->app->singleton('pay.log', function ($app) {
            $writer = new \Illuminate\Log\Writer(new \Monolog\Logger('pay'));
            $writer->useDailyFiles($app->storagePath() . '/logs/pay.log', 5);

            return $writer;
        });
    }
}

<?php

namespace App\Services;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Models\SocialAccount;

class SocialAccountService
{
    public function getUser(ProviderUser $providerUser, $provider = '') {
        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            if ($account->user && !empty($account->user->active)) {
                return $account->user;
            } else {
                return null;
            }
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);

            $account->save();

            return null;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: aku
 * Date: 14.07.18
 * Time: 11:35
 */

namespace App\Services;
use DrewM\MailChimp\MailChimp;


class MailChimpService
{
    public function send($email) {
        $mailChimp = new MailChimp(env('MAIL_API_KEY'));

        $mailChimp->post("lists/".env('MAIL_LIST_ID')."/members", [
            'email_address' => $email,
            'status' => 'subscribed'
        ]);

        if ($mailChimp->success()) {
            return trans('mail.thanks');
        } else {
            return trans('mail.exists');
        }
    }
}
<?php

namespace App\Services;

use App\Models\User;
use App\Models\CRMUsersStatus;
use App\Models\TempUserPack;
use AltSolution\Admin\Models\Option;
use Carbon\Carbon;
use Google\Spreadsheet\SpreadsheetService;
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
use Google_Client;

class AmoCrmService
{

    private $user;
    private $subDomain;

    public function __construct()
    {
        $this->user = config('crm.user');
        $this->subDomain = config('crm.subdomain');

        $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/auth.php?type=json', $this->user, 'Authorization!');
    }

    public function clearData($data) {
        return addslashes(strip_tags(trim($data)));
    }

    public function saveLeadActions()
    {
        $cmsUsersList = CRMUsersStatus::where('crm_user_status.status', 0)
            ->where('crm_user_status.user_id_lead', 0)
            ->join('users', 'crm_user_status.user_id', '=', 'users.id')
            ->leftJoin('orders', function ($join) {
                $join->on('crm_user_status.user_id', '=', 'orders.user_id');
            })
            ->leftJoin('utm_users', 'crm_user_status.user_id', '=', 'utm_users.user_id')
            ->select('crm_user_status.*', 'utm_users.utm_arr as utm_arr', 'utm_users.roistat_visit as roistat_visit', 'utm_users.client_id as client_id', 'orders.package_name as package_name', 'orders.payed as payed', 'users.name as user_name', 'users.id as user_p_ID', 'users.email as user_email', 'users.phone as user_phone', 'users.site as sitename', 'users.promocode as promocode')
            ->orderBy('orders.created_at', 'desc')
            ->get();

        if(count($cmsUsersList) > 0) {
            $leads = [];
            foreach ($cmsUsersList as $value) {

                //Get package name from cookies
                $package_name_l = '';
                if($value->package_name == '') {
                    $cn = TempUserPack::query()->where('id_user', $value->user_p_ID);
                    if($cn) {
                        $package_name_l = $cn->pluck('name_pack')->first();
                        //$cn->delete();
                    }
                }

                if ($value->package_name == "PREMIUM") {
                    $value->package_name = "SPECIAL OFFER";
                }

                //Set leads
                $arrUTM = [];
                if($value->utm_arr) {
                    $arrUTM = json_decode($value->utm_arr, true);
                }

              /*  $name  = $this->clearData($value->user_name);
                $email = $this->clearData($value->user_email);
                $phone = $this->clearData($value->user_phone);

                $formName = $this->clearData(($value->package_name != '') ? 'Пакет ' . $value->package_name : 'Бесплатный урок: Регистрация '. $package_name_l);
                $room     = '';

                $utmSource   = $this->clearData(array_get($arrUTM, 'utm_source', '-'));
                $utmMedium   = $this->clearData(array_get($arrUTM, 'utm_medium', '-'));
                $utmCampaign = $this->clearData(array_get($arrUTM, 'utm_campaign', '-'));
                $utmTerm     = $this->clearData(array_get($arrUTM, 'utm_term', '-'));
                $utmContent  = $this->clearData(array_get($arrUTM, 'utm_content', '-'));


                $roistatData = array(
                    'roistat' => isset($value->roistat_visit) ? $value->roistat_visit : '',
                    'key'     => 'MTUxNDcyOjkzMTk0OmI5NzE4N2E0ZGIxMDZhODVhYTE3ODBlMGExMGQ4NWYx', // Ключ для интеграции с CRM, указывается в настройках интеграции с CRM.
                    'title'   => isset($formName) ? $formName : "", // Название сделки
                    'comment' => 'коментарий', // Комментарий к сделке
                    'name'    => isset($name) ? $name : "", // Имя клиента
                    'email'   => isset($email) ? $email : "", // Email клиента
                    'phone'   => isset($phone) ? $phone : "", // Номер телефона клиента
                    'is_need_callback' => '0', // После создания в Roistat заявки, Roistat инициирует обратный звонок на номер клиента, если значение параметра рано 1 и в Ловце лидов включен индикатор обратного звонка.
                    'callback_phone'   => '', // Переопределяет номер, указанный в настройках обратного звонка.
                    'sync'             => '0', //
                    'is_need_check_order_in_processing'        => '1', // Включение проверки заявок на дубли
                    'is_need_check_order_in_processing_append' => '1', // Если создана дублирующая заявка, в нее будет добавлен комментарий об этом
                    'fields'  => array(
                        'responsible_user_id' => 2350324,
                        '535675'      => $value->package_name,
                        '517403'    => $utmSource,
                        '517409'    => $utmMedium,
                        '517413'    => $utmCampaign,
                        '517415'    => $utmTerm,
                        '517431'    => $utmContent,
                        // Массив дополнительных полей. Если дополнительные поля не нужны, оставьте массив пустым.
                        // Примеры дополнительных полей смотрите в таблице ниже.
                        "charset"   => "UTF-8", // Сервер преобразует значения полей из указанной кодировки в UTF-8.
                    ),
                );

                file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
                */
                $leads['request']['leads']['add'][] = [
                    'name' => ($value->package_name != '') ? 'Регистрация Персональный Бренд -25: Пакет ' . $value->package_name : 'Регистрация Персональный Бренд -25: Регистрация '. $package_name_l,
                    'date_create' => time(),
                    'status_id' => 18057247,
                    /*'tags' => $this->clearData($value->sitename) ?: '-',*/
                    'custom_fields' => [
                      /*  [
                            'id'     => 580328,
                            'values' => [
                                [
                                    'value' => $this->clearData($value->promocode) ?: '-',
                                    'enum' => 'CFV',
                                ],
                            ],
                        ], */
                        /*
                        * Name of product
                        * */
                        [
                            'id'     => 579548,
                            'values' => [
                                [
                                    'value' => 1188624,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        /*
                        * Source of deal
                        * */
                        [
                            'id'     => 579542,
                            'values' => [
                                [
                                    'value' => 1188586,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],

                        [
                            'id'     => 517403,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_source', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 517409,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_medium', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 517413,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_campaign', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 517415,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_term', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 517431,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_content', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 564317,
                            'values' => [
                                [
                                    'value' => $value->roistat_visit ?: '-' ,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 572961,
                            'values' => [
                                [
                                    'value' => $value->client_id ?: '-' ,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ]
                    ],
                ];

                putenv('GOOGLE_APPLICATION_CREDENTIALS='. storage_path() . '/my_secret.json');
                $client = new Google_Client();
                try{
                    $client->useApplicationDefaultCredentials();
                    $client->setApplicationName("PersonalBrandSiteRegistration");
                    $client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);
                    if ($client->isAccessTokenExpired()) {
                        $client->refreshTokenWithAssertion();
                    }

                    $accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
                    ServiceRequestFactory::setInstance(
                        new DefaultServiceRequest($accessToken)
                    );
                    // Get our spreadsheet
                    $spreadsheet = (new SpreadsheetService())
                        ->getSpreadsheetFeed()
                        ->getByTitle('PersonalBrandSiteRegistration');

                    // Get the first worksheet (tab)
                    $worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
                    $worksheet = $worksheets[0];

                    $listFeed = $worksheet->getListFeed();

                    $listFeed->insert([
                        'отметка'   => "'".date_create('now Europe/Kiev')->format('d-m-Y H:i:s'),
                        'дата'      => "'".date_create('now Europe/Kiev')->format('d-m-Y'),
                        'время'     => "'".date_create('now Europe/Kiev')->format('H:i:s'),
                        'фио'       => "'".$this->clearData($value->user_name),
                        'почта'     => "'".$this->clearData($value->user_email),
                        'телефон'   => "'".$this->clearData($value->user_phone),
                        'source'    => "'".$this->clearData(array_get($arrUTM, 'utm_source', '-')),
                        'medium'    => "'".$this->clearData(array_get($arrUTM, 'utm_medium', '-')),
                        'term'      => "'".$this->clearData(array_get($arrUTM, 'utm_term', '-')),
                        'campaign'  => "'".$this->clearData(array_get($arrUTM, 'utm_campaign', '-')),
                        'content'   => "'".$this->clearData(array_get($arrUTM, 'utm_content', '-')),
                        'sitename'  => "'".$this->clearData($value->sitename),
                     //   'promocode'  => "'".$this->clearData($value->promocode)
                    ]);

                }catch(Exception $e){
                    echo $e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile() . ' ' . $e->getCode();
                }
            }

            $leadsRes = $this->storeLead($leads);
            $leadsArrID = $leadsRes['response']['leads']['add'];

            //Save user_id_crm
            $crmIdIndex = 0;
            foreach ($cmsUsersList as $value) {
                $item = CRMUsersStatus::where('user_id', '=', $value->user_id)->first();
                if ($item) {
                    $item->user_id_lead = $leadsArrID[$crmIdIndex]['id'];
                    $item->save();
                    $crmIdIndex++;
                }
            }
            echo 'Data leads add'.PHP_EOL;
        } else {
            echo 'Data leads is actuality'.PHP_EOL;
        }
    }

    public function saveContactsActions()
    {
        $cmsUsersList = CRMUsersStatus::join('users', 'crm_user_status.user_id', '=', 'users.id')
            ->select('crm_user_status.*', 'users.name as user_name', 'users.email as user_email', 'users.phone as user_phone')
            ->orderBy('crm_user_status.created_at', 'desc')
            ->where('user_id_crm', 0)
            ->where('status', 0)
            ->get();

        if(count($cmsUsersList) > 0) {
            $contacts = [];
            foreach ($cmsUsersList as $value) {
                $contacts['request']['contacts']['add'][] = [

                    'name' => $value->user_name,
                    'linked_leads_id' => [
                        ($value->user_id_lead) ? $value->user_id_lead : 0,
                    ],
                    'custom_fields' => [
                        [
                            // Phones
                            'id' => 187645,
                            'values' => [
                                [
                                    'value' => $value->user_phone,
                                    'enum' => 'MOB',
                                ],
                            ],
                        ],
                        [
                            //Emails
                            'id' => 187647,
                            'values' => [
                                [
                                    'value' => $value->user_email,
                                    'enum' => 'PRIV',
                                ],
                            ],
                        ]
                    ],
                ];
            }
            $contactsRes = $this->storeContact($contacts);
            $contactsArrID = $contactsRes['response']['contacts']['add'];

            $crmIdIndex = 0;
            foreach ($cmsUsersList as $value) {
                $item = CRMUsersStatus::where('user_id', '=', $value->user_id)->first();
                if ($item) {
                    $item->user_id_crm = $contactsArrID[$crmIdIndex]['id'];
                    $item->save();
                    $crmIdIndex++;
                }
            }
            echo 'Data users add'.PHP_EOL;
        } else {
            echo 'Data users is actuality'.PHP_EOL;
        }

    }

    public function updateLeadsActions()
    {
        $dateNow = Carbon::now()->subDays(2)->timestamp;

        $cmsUsersNoPay = CRMUsersStatus::join('users', function ($join) use ($dateNow) {
            $join->on('crm_user_status.user_id', '=', 'users.id')
                ->where('users.last_login', '>=', $dateNow)
                ->where('users.sheets_flag', '!=', 1);
        })
            ->where('crm_user_status.status', 0)
            ->where('crm_user_status.user_id_lead', '!=', 102)
            ->leftJoin('orders', function ($join) {
                $join->on('crm_user_status.user_id', '=', 'orders.user_id');
            })
            ->where('orders.order_number', '!=' , 'create paid form admin')
            ->leftJoin('utm_users', 'crm_user_status.user_id', '=', 'utm_users.user_id')
            ->select('users.name as user_name', 'users.email as user_email', 'users.phone as user_phone', 'users.promocode as promocode', 'users.site as sitename', 'crm_user_status.*', 'utm_users.utm_arr as utm_arr', 'utm_users.roistat_visit as roistat_visit','utm_users.client_id as client_id', 'orders.payed as payed', 'users.id as user_p_ID', 'orders.package_name as package_name')
            ->orderBy('orders.created_at', 'desc')
            ->get();

        if(count($cmsUsersNoPay) > 0) {
            $leads = [];
            $iTimeTv = 1;
            foreach ($cmsUsersNoPay as $value) {
                $arrUTM = [];
                if($value->utm_arr) {
                    $arrUTM = json_decode($value->utm_arr, true);
                }

                putenv('GOOGLE_APPLICATION_CREDENTIALS='. storage_path() . '/my_secret.json');

                $client = new Google_Client();
                try{
                    $client->useApplicationDefaultCredentials();
                    $client->setApplicationName("PersonalBrandSiteGoToPay");
                    $client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);
                    if ($client->isAccessTokenExpired()) {
                        $client->refreshTokenWithAssertion();
                    }

                    $accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
                    ServiceRequestFactory::setInstance(
                        new DefaultServiceRequest($accessToken)
                    );
                    // Get our spreadsheet
                    $spreadsheet = (new SpreadsheetService())
                        ->getSpreadsheetFeed()
                        ->getByTitle('PersonalBrandSiteGoToPay');

                    // Get the first worksheet (tab)
                    $worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
                    $worksheet = $worksheets[0];

                    $listFeed = $worksheet->getListFeed();

                    $listFeed->insert([
                        'отметка'   => "'".date_create('now Europe/Kiev')->format('d-m-Y H:i:s'),
                        'дата'      => "'".date_create('now Europe/Kiev')->format('d-m-Y'),
                        'время'     => "'".date_create('now Europe/Kiev')->format('H:i:s'),
                        'фио'       => "'".$this->clearData($value->user_name),
                        'почта'     => "'".$this->clearData($value->user_email),
                        'телефон'   => "'".$this->clearData($value->user_phone),
                        'source'    => "'".$this->clearData(array_get($arrUTM, 'utm_source', '-')),
                        'medium'    => "'".$this->clearData(array_get($arrUTM, 'utm_medium', '-')),
                        'term'      => "'".$this->clearData(array_get($arrUTM, 'utm_term', '-')),
                        'campaign'  => "'".$this->clearData(array_get($arrUTM, 'utm_campaign', '-')),
                        'content'   => "'".$this->clearData(array_get($arrUTM, 'utm_content', '-')),
                        'sitename'  => "'".$this->clearData($value->sitename),
                     //   'promocode'  => "'".$this->clearData($value->promocode)
                    ]);
                }catch(Exception $e){
                    echo $e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile() . ' ' . $e->getCode();
                }

                $user = User::query()->where('email', '=', $value->user_email)->first();
                $user->sheets_flag = 1;
                $user->save();
                if ($value->package_name == "PREMIUM") {
                    $value->package_name = "SPECIAL OFFER";
                }

                //Get package name from cookies
                $package_name_l = '';
                if($value->package_name == '') {
                    $cn = TempUserPack::query()->where('id_user', $value->user_p_ID);
                    if($cn) {
                        $package_name_l = $cn->pluck('name_pack')->first();
                    }
                }

                $leads['request']['leads']['update'][] = [
                    'id' => $value->user_id_lead,
                    'last_modified' => time() + $iTimeTv,
                    'name' => ($value->package_name != '') ? 'Купить Персональный Бренд -25 : Пакет ' . $value->package_name : 'Купить Персональный Бренд -25 : Регистрация ' . $package_name_l,
                /*    'tags' => $this->clearData($value->sitename) ?: '-',*/
                    'custom_fields'   => [
                     /*   [
                            'id'     => 580328,
                            'values' => [
                                [
                                    'value' => $this->clearData($value->promocode) ?: '-',
                                    'enum' => 'CFV',
                                ],
                            ],
                        ], */
                        /*
                        * Name of product
                        * */
                        [
                            'id'     => 579548,
                            'values' => [
                                [
                                    'value' => 1188624,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        /*
                        * Source of deal
                        * */
                        [
                            'id'     => 579542,
                            'values' => [
                                [
                                    'value' => 1188586,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        //UTM
                        [
                            'id'     => 517403,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_source', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 517409,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_medium', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 517413,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_campaign', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 517415,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_term', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 517431,
                            'values' => [
                                [
                                    'value' => array_get($arrUTM, 'utm_content', '-'),
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 564317,
                            'values' => [
                                [
                                    'value' => $value->roistat_visit ?: '-' ,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 572961,
                            'values' => [
                                [
                                    'value' => $value->client_id ?: '-' ,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ]
                    ]
                ];
                $iTimeTv++;
            }
            $resUsC = $this->updateLead($leads);

            $errorArr = [];
            try {
                $errorArr = $resUsC['response']['leads']['update']['errors'];
            } catch (\Exception $E) {
                null;
            }
            $errorNotFound = [];
            if(count($errorArr) > 0) {
                foreach ($errorArr as $index => $value) {
                    if (strcmp(trim($value), 'Lead not found') == 0) {
                        $errorNotFound[] = $index;
                    }
                }

                foreach ($cmsUsersNoPay as $value) {
                    $idUser = $value->user_id;
                    $item = CRMUsersStatus::where('user_id', '=', $idUser)->first();

                    //Error empty lead.
                    if (count($errorNotFound) > 0) {
                        if (array_search($item->user_id_lead, $errorNotFound) !== false) {
                            logger()->info('(Name update) Lead not found. To null, code(102). User ID ' . $idUser);
                            $item->user_id_lead = 102;
                            $item->save();
                        }
                    }
                }
            }
            echo 'Update name packet'.PHP_EOL;
        }

        $cmsUsersList = CRMUsersStatus::join('users', function ($join) use ($dateNow) {
            $join->on('crm_user_status.user_id', '=', 'users.id')
                ->where('users.last_login', '>=', $dateNow)
                ->where('users.sheets_flag', '!=', 2);
        })
            ->join('orders', function ($join) {
                $join->on('crm_user_status.user_id', '=', 'orders.user_id')
                    ->where('orders.payed', '=', 1)
                    ->where('crm_user_status.status', '=', 0)
                    ->where('crm_user_status.user_id_lead', '!=', 102);
            })
            ->where('orders.order_number', '!=' , 'create paid form admin')
            ->leftJoin('utm_users', 'crm_user_status.user_id', '=', 'utm_users.user_id')
            ->select('users.name as user_name', 'users.email as user_email', 'users.phone as user_phone', 'users.promocode as promocode', 'users.site as sitename', 'crm_user_status.*', 'utm_users.roistat_visit as roistat_visit', 'utm_users.client_id as client_id', 'orders.payed as payed', 'orders.package_name as package_name', 'orders.order_number as order_number', 'orders.price as price')
            ->orderBy('orders.created_at', 'desc')
            ->get();

        if(count($cmsUsersList) > 0) {
            $option = Option::query();
            $leadsUs = [];
            $iTime = 50;
            foreach ($cmsUsersList as $value) {
                $sale = 0;
                switch($value->package_name) {
                    case 'GOOD':
                        $sale = $option->where('name', 'course_package_cheap_price')->pluck('value')->first();
                        break;
                    case 'BEST':
                        $sale = $option->where('name', 'course_package_good_price')->pluck('value')->first();
                        break;
                    case 'PREMIUM':
                        $sale = $option->where('name', 'course_package_premium_price')->pluck('value')->first();
                        break;
                }

                $arrUTM = [];
                if($value->utm_arr) {
                    $arrUTM = json_decode($value->utm_arr, true);
                }

                putenv('GOOGLE_APPLICATION_CREDENTIALS='. storage_path() . '/my_secret.json');

                $client = new Google_Client();
                try{
                    $client->useApplicationDefaultCredentials();
                    $client->setApplicationName("PersonalBrandSitePaid");
                    $client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);
                    if ($client->isAccessTokenExpired()) {
                        $client->refreshTokenWithAssertion();
                    }

                    $accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
                    ServiceRequestFactory::setInstance(
                        new DefaultServiceRequest($accessToken)
                    );
                    // Get our spreadsheet
                    $spreadsheet = (new SpreadsheetService())
                        ->getSpreadsheetFeed()
                        ->getByTitle('PersonalBrandSitePaid');

                    // Get the first worksheet (tab)
                    $worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
                    $worksheet = $worksheets[0];

                    $listFeed = $worksheet->getListFeed();

                    $listFeed->insert([
                        'отметка'   => "'".date_create('now Europe/Kiev')->format('d-m-Y H:i:s'),
                        'дата'      => "'".date_create('now Europe/Kiev')->format('d-m-Y'),
                        'время'     => "'".date_create('now Europe/Kiev')->format('H:i:s'),
                        'фио'       => "'".$this->clearData($value->user_name),
                        'почта'     => "'".$this->clearData($value->user_email),
                        'телефон'   => "'".$this->clearData($value->user_phone),
                        'source'    => "'".$this->clearData(array_get($arrUTM, 'utm_source', '-')),
                        'medium'    => "'".$this->clearData(array_get($arrUTM, 'utm_medium', '-')),
                        'term'      => "'".$this->clearData(array_get($arrUTM, 'utm_term', '-')),
                        'campaign'  => "'".$this->clearData(array_get($arrUTM, 'utm_campaign', '-')),
                        'content'   => "'".$this->clearData(array_get($arrUTM, 'utm_content', '-')),
                        'sitename'  => "'".$this->clearData($value->sitename),
                     //   'promocode'  => "'".$this->clearData($value->promocode)
                    ]);
                }catch(Exception $e){
                    echo $e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile() . ' ' . $e->getCode();
                }

                $user = User::query()->where('email', '=', $value->user_email)->first();
                $user->sheets_flag = 2;
                $user->save();

                /*$name  = $this->clearData($value->user_name);
                $email = $this->clearData($value->user_email);
                $phone = $this->clearData($value->user_phone);

                $formName = $this->clearData(($value->package_name != '') ? 'Пакет ' . $value->package_name : 'Бесплатный урок: Регистрация '. $package_name_l);
                $room     = '';

                $utmSource   = $this->clearData(array_get($arrUTM, 'utm_source', '-'));
                $utmMedium   = $this->clearData(array_get($arrUTM, 'utm_medium', '-'));
                $utmCampaign = $this->clearData(array_get($arrUTM, 'utm_campaign', '-'));
                $utmTerm     = $this->clearData(array_get($arrUTM, 'utm_term', '-'));
                $utmContent  = $this->clearData(array_get($arrUTM, 'utm_content', '-'));


                $roistatData = array(
                    'roistat' => isset($value->roistat_visit) ? $value->roistat_visit : '',
                    'key'     => 'MTUxNDcyOjkzMTk0OmI5NzE4N2E0ZGIxMDZhODVhYTE3ODBlMGExMGQ4NWYx', // Ключ для интеграции с CRM, указывается в настройках интеграции с CRM.
                    'title'   => isset($formName) ? $formName : "", // Название сделки
                    'comment' => 'коментарий', // Комментарий к сделке
                    'name'    => isset($name) ? $name : "", // Имя клиента
                    'email'   => isset($email) ? $email : "", // Email клиента
                    'phone'   => isset($phone) ? $phone : "", // Номер телефона клиента
                    'is_need_callback' => '0', // После создания в Roistat заявки, Roistat инициирует обратный звонок на номер клиента, если значение параметра рано 1 и в Ловце лидов включен индикатор обратного звонка.
                    'callback_phone'   => '', // Переопределяет номер, указанный в настройках обратного звонка.
                    'sync'             => '0', //
                    'is_need_check_order_in_processing'        => '1', // Включение проверки заявок на дубли
                    'is_need_check_order_in_processing_append' => '1', // Если создана дублирующая заявка, в нее будет добавлен комментарий об этом
                    'fields'  => array(
                        'price' => (int)$sale,
                        'responsible_user_id' => 2350324,
                        '535675'      => $value->package_name,
                        '517403'    => $utmSource,
                        '517409'    => $utmMedium,
                        '517413'    => $utmCampaign,
                        '517415'    => $utmTerm,
                        '517431'    => $utmContent,
                        // Массив дополнительных полей. Если дополнительные поля не нужны, оставьте массив пустым.
                        // Примеры дополнительных полей смотрите в таблице ниже.
                        "charset"   => "UTF-8", // Сервер преобразует значения полей из указанной кодировки в UTF-8.
                    ),
                );

                file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
                */

                if ($value->package_name == "PREMIUM") {
                    $value->package_name = "SPECIAL OFFER";
                }

                $leadsUs['request']['leads']['update'][] = [
                    'id' => $value->user_id_lead,
                    'last_modified' => time() + $iTime,
                    'status_id' => 142,
                    'price' =>  (int)$value->price, // цена в гривнах
                    'tags' => ($value->order_number == 'create paid form admin') ? 'отдел продаж' : 'автооплата',/*.', '.$this->clearData($value->sitename) ?: '-',*/
                    'custom_fields'   => [
                      /*  [
                            'id'     => 580328,
                            'values' => [
                                [
                                    'value' => $this->clearData($value->promocode) ?: '-',
                                    'enum' => 'CFV',
                                ],
                            ],
                        ], */
                        /*
                          * Name of product
                          * */
                        [
                            'id'     => 579548,
                            'values' => [
                                [
                                    'value' => 1188624,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        /*
                        * Source of deal
                        * */
                        [
                            'id'     => 579542,
                            'values' => [
                                [
                                    'value' => 1188586,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            // Пакет
                            'id'     => 535675,
                            'values' => [
                                [
                                    'value' => $value->package_name,
                                    'enum'  => 'CFV',
                                ],
                            ],
                        ],
                        [
                            // Цена в USD
                            'id'     => 576804,
                            'values' => [
                                [
                                    'value' => (int)$sale,
                                    'enum'  => 'CFV',
                                ],
                            ],
                        ],
                        [
                            // Оплата
                            'id'     => 542805,
                            'values' => [
                                [
                                    'value' => "Сайт",
                                    'enum'  => 'CFV',
                                ],
                            ]
                        ],
                        [
                            // Оплата
                            'id'     => 564317,
                            'values' => [
                                [
                                    'value' => $value->roistat_visit ?: '-' ,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ],
                        [
                            'id'     => 572961,
                            'values' => [
                                [
                                    'value' => $value->client_id ?: '-' ,
                                    'enum' => 'CFV',
                                ],
                            ],
                        ]
                    ]
                ];
                $iTime++;
            }

            $resUs = $this->updateLead($leadsUs);

            $errorArr = [];
            try {
                $errorArr = $resUs['response']['leads']['update']['errors'];
            } catch (\Exception $E) {
                null;
            }
            $errorNotFound = [];
            $errorLastMod = [];
            if(count($errorArr) > 0) {
                foreach($errorArr as $index=>$value) {
                    if(strcmp(trim($value), 'Lead not found') == 0) {
                        //logger()->info('Lead not found. User id Lead(CRM) ' . $index);
                        $errorNotFound[] = $index;
                    }
                    if(strcmp(trim($value), 'Last modified date is older than in database') == 0) {
                        //logger()->info('Last modified date is older than in database. User id Lead(CRM) ' . $index);
                        $errorLastMod[] = $index;
                    }
                }
            }

            foreach ($cmsUsersList as $value) {
                $idUser = $value->user_id;
                $item = CRMUsersStatus::where('user_id', '=', $idUser)->first();

                //Error last mod.
                if(count($errorLastMod) > 0) {
                    if (array_search($item->user_id_lead, $errorLastMod) !== false)
                        logger()->info('Last modified date is older than in database. User ID ' . $idUser);
                    continue;
                }

                //Error empty lead.
                if(count($errorNotFound) > 0) {
                    if (array_search($item->user_id_lead, $errorNotFound) !== false) {
                        $item->user_id_lead = 102;
                        $item->save();
                        logger()->info('Lead not found. To null, code(102). User ID ' . $idUser);
                        continue;
                    }
                }

                if ($item) {
                    $item->status = 1;
                    $item->save();
                }
            }
            echo 'Data update'.PHP_EOL;

        } else {
            echo 'Not data to update'.PHP_EOL;
        }
    }

    /**
     *
     */
    public function syncUsers()
    {
        $crm = new CRMUsersStatus();
        $lastID = $crm::orderBy('user_id', 'desc')->pluck('user_id')->first();
        if($lastID) {
            $users = User::query()->where('acl_role_id', 2)->where('active', 1)->where('id', '>', $lastID)->get();
        } else {
            $users = User::query()->where('acl_role_id', 2)->where('active', 1)->get();
        }

        if(count($users) > 0) {
            $arrUsers = [];
            foreach ($users as $value) {
                $arrUsers[] = [
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'user_id' => $value->id
                ];
            }
            $crm->insert($arrUsers);

            echo 'Users update'.PHP_EOL;
        } else {
            echo 'Users data is actuality'.PHP_EOL;
        }
    }

    /**
     * @param $link
     * @param $data
     * @param null $operation
     * @return mixed
     */
    private function curlInit($link, $data, $operation = null)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        $code=(int)$code;
        $errors=array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable',
            504=>'Gateway Timeout'
        );
        try
        {
            #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
            if($code!=200 && $code!=204) {
                throw new \Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
            }
        }
        catch(\Exception $E)
        {
            logger()->error('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode() .PHP_EOL. 'Этап запроса: ' . $operation);
        }

        return json_decode($out, true);
    }


    /**
     * @param $leads
     * @return mixed
     */
    private function storeLead($leads)
    {
        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/leads/set', $leads, 'Save Leads!');
    }

    /**
     * @param $leads
     * @return mixed
     */
    public function updateLead($leads)
    {
        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/leads/set', $leads, 'Update Leads!');
    }


    /**
     * @param $contacts
     * @return mixed
     */
    private function storeContact($contacts)
    {
        return $this->curlInit('https://' . $this->subDomain . '.amocrm.ru/private/api/v2/json/contacts/set', $contacts, 'Save Contacts!');
    }
}
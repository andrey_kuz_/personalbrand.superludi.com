<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UTMUsers extends Model
{
    protected $table = 'utm_users';
    protected $fillable = [
        'user_id',
        'utm_arr',
        'status',
        'roistat_visit'
    ];
}

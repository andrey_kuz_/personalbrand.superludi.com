<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Package;
use App\Currency;

class Order extends Model
{
    protected $fillable = ['user_id', 'order_number', 'package_name', 'payed', 'payed_at', 'price', 'created_at', 'updated_at'];

    public function user() {
        return $this->belongsTo(\App\Models\User::class);
    }
	
	public function baskets() {
        return $this->hasMany(\App\Models\Basket::class);
    }
	
	public function scopePayed($query) {
        return $query->where('payed', 1);
    }
	
	public function scopeUnpayed($query) {
        return $query->where('payed', 0);
    }
	
	static public function unpaidOrderByUnpaidVideos(User $user, $unpaid_videos)
	{
		if(intval($user->id) <= 0 || !is_array($unpaid_videos) || empty($unpaid_videos))
		{
			return null;
		}	
		
		foreach($user->orders()->unpayed()->get() as $order)
		{
			$basket = $order->baskets()->lists('video_id')->toArray();
			if(empty(array_diff($unpaid_videos, $basket)))
			{
				return $order;
			}		
		}
		
		return null;
	}		

	static public function infoPay($user_id, $order_id, $package_code)
	{
		$result = [];
		$logger = app('pay.log');
		
		$user_id = intval($user_id);
		$order_id = intval($order_id);
		if(intval($user_id) <= 0 || intval($order_id) <= 0)
		{
			$logger->info('Order/infoPay: ошибка входных данных');
			return $result;
		}
		
		$packages = Package::availables();
		if(empty($packages) || empty($packages[$package_code]) || empty($packages[$package_code]['price']))
		{
			$logger->info('Order/infoPay: ошибка получения цены пакета');
			return $result;
		}
		
		$videos_count = Basket::where('order_id', $order_id)->count();
		if($videos_count <= 0)
		{
			$logger->info('Order/infoPay: ошибка получения корзины заказа order_id='.$order_id.', user_id='.$user_id.', package_code='.$package_code);
			return $result;
		}	
		
		$course_price = $packages[$package_code]['price'];
		$course_price_rate = cms_option('course_price_rate');
		if(empty($course_price) || empty($course_price_rate))
		{
			$logger->info('Order/infoPay: ошибка формирования цены заказа order_id='.$order_id.', user_id='.$user_id.', package_code='.$package_code.', package_price='.$course_price.', course_price_rate='.$course_price_rate);
			return $result;
		}

        $name = !empty($packages[$package_code]['name']) ? trans('order.package_name', ['package_name' => $packages[$package_code]['name']]) : trans('order.full_course');
        if ($name == 'Персональный Пакет "PREMIUM"') {
            $name = 'Персональный Пакет "SPECIAL OFFER"';
        }
        $number = $user_id.'-'.$order_id;
        $price = ($course_price * $course_price_rate * 100) / 97.7;
        $ext1 = number_format(($course_price * 100) / 97.7, 2, '.', '').' '.Currency::USA;

        $result = [
			'name' => $name,
			'number' => $number,
			'price' => $price,
			'ext1' => $ext1,
		];
		
        $logger->info('Order/infoPay', ['user_id' => $user_id, 'order_id' => $order_id, 'package_code' => $package_code, 'package_price' => $course_price, 'course_price_rate' => $course_price_rate] + $result);
		
		return $result;
	}
	
	static public function parserOrderId($order_id)
	{
		if(empty($order_id) || !is_string($order_id))
		{
			return [0, []];
		}
		
		$order_id_parts = explode('-', $order_id);
		if(count($order_id_parts) < 2 || empty($order_id_parts[0]) || empty($order_id_parts[1]))
		{
			return [0, []];
		}
		
		return [$order_id_parts[0], $order_id_parts[1]];
	}
	
	public function purchased($payment_data)
	{
		if(empty($this->baskets) || empty($this->user->id))
		{
			return false;
		}
		
		$this->payed = true;
		$this->payed_at = new Carbon;
		$this->history .= "\n".'['.strval(new Carbon).'] Заказ оплачен';
		$this->history .= "\n".'Пользователь: имя - '.$this->user->name.' id - '.$this->user->id.' email - '.$this->user->email;
		$this->history .= "\n".'Данные платежной системы '.print_r($payment_data, TRUE);
		$this->history .= "\n";
		
		return $this->save();
	}
	
	public function refund($payment_data)
	{
		if(empty($this->baskets) || empty($this->user->id))
		{
			return false;
		}
		
		$this->payed = false;
		$this->history .= "\n".'['.strval(new Carbon).'] Возврат заказа';
		$this->history .= "\n".'Пользователь: имя - '.$this->user->name.' id - '.$this->user->id.' email - '.$this->user->email;
		$this->history .= "\n".'Данные платежной системы '.print_r($payment_data, TRUE);
		$this->history .= "\n";
		
		return $this->save();
	}		
}	
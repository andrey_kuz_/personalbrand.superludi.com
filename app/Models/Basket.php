<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    public $timestamps = false;
	protected $fillable = ['order_id', 'video_id'];

	public function video() {
        return $this->belongsTo(\App\Models\Video::class);
    }
	

}	
<?php

namespace App\Models;

use AltSolution\Admin\Models\User as BaseUser;
use Illuminate\Support\Facades\Hash;
use App\Models\Video;

class User extends BaseUser
{
	public function getFillable()
    {
        $fillable = parent::getFillable();

        $fillable[] = 'phone';
		$fillable[] = 'password';
        $fillable[] = 'site';
        $fillable[] = 'promocode';
        return $fillable;
    }

    protected $appends = ['paid'];

    public function getPaidAttribute() {
        $order = Order::where('user_id', $this->id)->where('payed', true)->first();
        if($order) {
           return true;
        }
        return false;
    }
	
	public function orders() {
        return $this->hasMany(\App\Models\Order::class);
    }
    public function videoPoint() {
        return $this->hasOne(\App\Models\VideoPoint::class);
    }
	
	public function shortName()
	{
		$result = '';
		if(empty($this->name))
		{
			return $result;
		}	
		
		$name = mb_convert_case(trim($this->name), MB_CASE_TITLE, "UTF-8");
		$name = explode(' ', $name);
		foreach($name as $word)
		{
			if(empty($word))
			{
				continue;
			}
			
			$result .= mb_substr($word, 0, 1);
			if(mb_strlen($result, 'UTF-8') == 2)
			{
				break;
			}		
		}	
		
		return $result;
	}	
	
	static public function isPayedByEmail($email)
	{
		if(empty($email))
		{
			return FALSE;
		}
		
		$user = User::where('email', $email)->first();
		
		return is_object($user) && $user->id > 0 && count($user->paidVideos()); 
	}
	
	static public function socialAuthError()
	{
		return session()->has('social_auth_error') ? session('social_auth_error') : '';
	}
	
	static public function updatePassword($email, $password)
	{
		if(empty($email) || empty($password))
		{
			return FALSE;
		}
		
		$user = User::where('email', $email)->first();
		if(empty($user))
		{
			return FALSE;
		}
		
		if(!Hash::check($password, $user->password_reset))
		{
			return FALSE;
		}		
		
		$user->password = $user->password_reset;
		
		return $user->save();
	}		

	public function paidVideos()
	{
		$result = [];
		
		$videos_paid = [];
		$orders = $this->orders()->payed()->get();

		foreach($orders as $order)
		{
			$videos_paid = array_merge($videos_paid, $order->baskets()->lists('video_id')->toArray());

		}
		$videos_paid = array_unique($videos_paid);

		
		if(empty($videos_paid))
		{
			return $result;
		}	
		
		$video_ids = Video::videosAvailable()->lists('id')->toArray();

		if(empty($video_ids))
		{
			return $result;
		}	
		
		foreach($videos_paid as $video_paid_id)
		{
			if(empty($video_paid_id) || !in_array($video_paid_id, $video_ids))
			{
				continue;
			}
			
			$result[] = $video_paid_id;
		}	
		
		return $result;
	}

	public function unpaidVideos()
	{
		$video_ids = Video::videosAvailable()->lists('id')->toArray();
		$user_order_ids = $this->paidVideos();
		
		return array_diff($video_ids, $user_order_ids);
	}
	
	public function scopeActive($query) {
        return $query->where('active', 1);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CRMUsersStatus extends Model
{
    protected $table = 'crm_user_status';
    protected $fillable = [
        'user_id',
        'user_id_crm',
        'user_id_lead',
        'status'
    ];
}

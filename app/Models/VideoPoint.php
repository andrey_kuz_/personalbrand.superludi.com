<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoPoint extends Model
{
    protected $fillable = [
        'user_id',
        'video_id',
        'time',
    ];
    public function users()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}

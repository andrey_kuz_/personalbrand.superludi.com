<?php

namespace App\Models;
use App\Currency;

class Pay 
{
    static public function form($order)
	{
		$logger = app('pay.log');
		
		if(empty($order['name']) || empty($order['number']) || empty($order['price']))
		{
			return [];
		}
		
		$order['price'] = floatval($order['price']);
		if(!is_float($order['price']))
		{
			return [];
		}	
		
		$key = config('platon.key');
		$payment = 'CC';
		$order_data = [
            'amount' => number_format($order['price'], 2, '.', ''),
            'description' => $order['name'],
            'currency' => Currency::UKRAINE, //на территории Украины законно принимать только гривну
        ];
        $data = base64_encode(json_encode($order_data));
        $sign = md5(strtoupper(
            strrev($key).
            strrev($payment).
            strrev($data).
            strrev(route('pay_success')).
            strrev(config('platon.pass'))
        ));
		
		$logger->info('Запрос на оплату', [
			'order_id' => $order['number'],
			'order_data' => $order_data,
			'url_success' => route('pay_success'),
			'url_fail' => route('pay_fail'),
			'action' => config('platon.url'),
			'payment' => $payment,
		]);
        return [
            'action' => config('platon.url'),
			'key' => $key,
            'payment' => $payment,
            'order' => $order['number'],
            'data' => $data,
            'sign' => $sign,
			'ext1' => $order['ext1'],
        ];
	}
	
	static public function fail()
	{
		return session()->has('payment_error') ? session('payment_error') : '';
	}
	
	static public function success()
	{
		return session()->has('url_purchased_video') ? session('url_purchased_video') : '';
	}
	
	static public function noConfirmation()
	{
		return session()->has('no_payment_confirmation') ? session('no_payment_confirmation') : '';
	}
}

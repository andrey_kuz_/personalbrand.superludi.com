<?php

namespace App\Models;

use AltSolution\Admin\Helpers\ImagesInterface;
use AltSolution\Admin\Helpers\ImagesTrait;
use AltSolution\Admin\Helpers\SeoInterface;
use AltSolution\Admin\Helpers\SeoTrait;
use AltSolution\Admin\Helpers\TranslateTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Example extends Model implements SeoInterface, ImagesInterface
{
    use ImagesTrait;
    use TranslateTrait;
    use SeoTrait;

    protected $fillable = [
        'title_*',
        'content_*',
        'permalink',
        'is_published',
        'published_at',
        'image',
    ];

    public function getImagesFields()
    {
        return [
            'image' => [
                'list' => ['resize', 100, 100],
                //
            ],
        ];
    }

    public function getPublishedAtAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = Carbon::createFromFormat('m/d/Y',$value);
    }

    public function scopeSearch($query, $text)
    {
        return $query->where(function ($query) use ($text) {
            $locale = config('app.locale');
            $query->orWhere('title_' . $locale, 'LIKE', '%' . $text . '%');
        });
    }
}

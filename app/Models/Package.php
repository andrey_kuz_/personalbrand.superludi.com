<?php

namespace App\Models;

class Package
{
	static public function all()
	{
		return [
			'cheap' => [
				'code' => 'cheap',
				'price' => 0,
				'name' => 'GOOD',
				'description' => '',
			],
			'good' => [
				'code' => 'good',
				'price' => 0,
				'name' => 'BEST',
				'description' => '',
			],
			'premium' => [
				'code' => 'premium',
				'price' => 0,
				'name' => 'PREMIUM',
				'description' => '',
			],
		];
		
	}
	
	static public function availables()
	{
		$packages = self::all();
		if(empty($packages))
		{
			return [];
		}	
		
		foreach($packages as &$package)
		{
			$package_price = cms_option('course_package_'.$package['code'].'_price');
			if(empty($package_price))
			{
				unset($packages[$package['code']]);
				continue;
			}
			
			$package['price'] = $package_price;
		}	
		unset($package);
		
		return $packages;
	}
	
	static public function toPay()
	{
		return session()->has('package_to_pay') ? session('package_to_pay') : '';
	}
	
	static public function byName($name)
	{
		if(empty($name))
		{
			return [];
		}	
		
		$packages = self::availables();
		if(empty($packages))
		{
			return [];
		}
		
		foreach($packages as $package)
		{
			if(!empty($package['name']) && $package['name'] == $name)
			{
				return $package;
			}	
		}	
		
		return [];
	}
}

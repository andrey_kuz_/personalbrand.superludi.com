<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examples', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $locales = ['en'];
            foreach ($locales as $locale) {
                $table->string('title_' . $locale);
                $table->string('content_' . $locale);
            }
            
            $table->string('permalink');
            $table->boolean('is_published');
            $table->timestamp('published_at')->nullable();
            $table->string('image');

            $table->unsignedInteger('sort');
            $table->index('sort', 'idx_sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('examples');
    }
}

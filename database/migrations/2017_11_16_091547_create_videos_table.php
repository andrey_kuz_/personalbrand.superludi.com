<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
	/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            
            $locale = config('app.locale');
			if(!empty($locale))
			{
				$table->string('title_' . $locale);
                $table->text('content_' . $locale); 
			}  
            $table->string('permalink');
			$table->string('image');
			$table->string('file');
			$table->text('code');
            $table->boolean('is_published');
            $table->timestamp('published_at')->nullable();
			
			$table->timestamps();
            $table->unsignedInteger('sort');
            $table->index('sort', 'idx_sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}

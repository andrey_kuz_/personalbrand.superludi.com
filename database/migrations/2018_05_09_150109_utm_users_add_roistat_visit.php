<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UtmUsersAddRoistatVisit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('utm_users', function (Blueprint $table) {
            $table->integer('roistat_visit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('utm_users', function($table)
        {
            $table->dropColumn('roistat_visit');
        });
    }
}

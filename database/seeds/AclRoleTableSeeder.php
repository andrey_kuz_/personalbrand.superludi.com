<?php

use Illuminate\Database\Seeder;

class AclRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('acl_roles')->insert([
        	'name' => 'guest',
        	'description' => 'Guest user',
        ]);
        DB::table('acl_roles')->insert([
            'name' => 'login',
            'description' => 'Logged user',
        ]);
        $roleId = DB::table('acl_roles')->insertGetId([
            'name' => 'admin',
            'description' => 'Administrator',
        ]);

        $permission1 = DB::table('acl_permissions')->insertGetId([
            'name' => 'admin',
            'description' => 'admin::core.p_admin',
        ]);
        $permission2 = DB::table('acl_permissions')->insertGetId([
            'name' => 'role.edit',
            'description' => 'admin::user.p_role_edit',
        ]);
        $permission3 = DB::table('acl_permissions')->insertGetId([
            'name' => 'user.list',
            'description' => 'admin::user.p_user_list',
        ]);

        DB::table('acl_permissions_acl_roles')->insert([
            'acl_role_id' => $roleId,
            'acl_permission_id' => $permission1,
        ]);
        DB::table('acl_permissions_acl_roles')->insert([
            'acl_role_id' => $roleId,
            'acl_permission_id' => $permission2,
        ]);
        DB::table('acl_permissions_acl_roles')->insert([
            'acl_role_id' => $roleId,
            'acl_permission_id' => $permission3,
        ]);
    }
}

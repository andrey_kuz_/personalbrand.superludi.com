# CMS quick start app

## Download

[archive.zip](http://gl.altsolution.net/altsolution/cms-app/repository/archive.zip?ref=master) and unpack

## Usage

Install php libraries
```
composer install --no-dev
```

Move admin assets to public folder
```
php artisan vendor:publish --tag=admin-assets --force
```

Move and apply database migration
```
php artisan vendor:publish --tag=admin-migrations
php artisan migrate --seed
```

